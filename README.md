# Commons

This project is actually just to simplify my work. It combines different, frequently used functions in an intigrated
system. Mostly its just an abstraction of other libs, some times there are some self coded solutions.
## Tech

This project uses a number of open source projects to work properly, have a look in the pom file to see them all.
However, there are some not integrated resources of the  University of Darmstadt which are accessable on their web page and required for this project: https://www.flexiprovider.de/
Also ASN.1 is required and available on SourceForge: https://sourceforge.net/projects/codec/
Use the startup.sh to install this requirements on your system!

By the way: This project is based on Oracle Java 9

## Some examples

### Crypto
#### Asymmetric signing
```java
byte[] message = "Hallo Welt".getBytes();
AsymmetricKeyPair pair = Keys.asymmetricPairs().mcEliece().size(128).create();

byte[] signature = Signers.asymmetric().mcEliece(pair.getSecret()).asBytes(message);

System.out.println(Signers.asymmetric().mcEliece(pair.getShared()).verify(message, signature));
```

### Asymmetric encryption/decryption
```java
byte[] message = "Hallo Welt".getBytes();
AsymmetricKeyPair pair = Keys.asymmetricPairs().mcEliece().size(128).create();

byte[] enc = Ciphers.asymmetric().mcEliece(pair.getShared()).encrypt(message);

System.out.println(new String(Ciphers.asymmetric().mcEliece(pair.getSecret()).decrypt(enc)));
```

### Symmetric encryption/decryption
```
byte[] message = "Hallo World".getBytes();
SymmetricKey internalKey = Keys.symmetric().generate();

byte[] enc = Ciphers.symmetric().aes(internalKey).encrypt(message);
System.out.println(new String(Ciphers.symmetric().aes(internalKey).decrypt(enc)));
```

## Serialisation/Deserialisation
### Serialisation
```java
Map<String, String> source = new HashMap<String, String>();
source.put("Hallo", "World");

String json = Encoders.json().create().encode(source).asString();
String yaml = Encoders.yaml().encode(source).asString();
```