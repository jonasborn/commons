package de.obdev.commons.web.url.implementation;

import de.obdev.commons.model.ModelArray;
import de.obdev.commons.model.ModelElement;
import de.obdev.commons.model.ModelPrimitive;
import de.obdev.commons.web.url.BasicUrlDesigner;

public class SeperatedUrlDesigner extends BasicUrlDesigner {
  @Override
  public String createList(String key, ModelArray array) {
    StringBuilder builder = new StringBuilder();
    builder.append(key).append("=");
    for (ModelElement element : array) {
      if (element instanceof ModelPrimitive) builder.append(stringify(element));
      builder.append(", ");
    }
    if (builder.length() > 2) builder.delete(builder.length() - 2, builder.length());
    return builder.toString();
  }
}
