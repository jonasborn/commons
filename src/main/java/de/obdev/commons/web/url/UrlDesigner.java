package de.obdev.commons.web.url;

import de.obdev.commons.model.Model;

public interface UrlDesigner {

  public String design(Model model);
}
