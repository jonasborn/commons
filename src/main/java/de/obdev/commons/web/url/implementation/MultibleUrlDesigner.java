package de.obdev.commons.web.url.implementation;

import de.obdev.commons.model.ModelArray;
import de.obdev.commons.model.ModelElement;
import de.obdev.commons.model.ModelPrimitive;
import de.obdev.commons.web.url.BasicUrlDesigner;

public class MultibleUrlDesigner extends BasicUrlDesigner {
  @Override
  public String createList(String key, ModelArray array) {
    StringBuilder builder = new StringBuilder();
    for (ModelElement element : array) {
      if (element instanceof ModelPrimitive) {
        builder.append(key).append("=");
        builder.append(stringify(element));
        builder.append("&");
      }
    }
    builder.deleteCharAt(builder.length() - 1);
    return builder.toString();
  }
}
