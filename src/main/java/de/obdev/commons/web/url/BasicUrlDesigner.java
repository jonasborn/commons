package de.obdev.commons.web.url;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.model.Model;
import de.obdev.commons.model.ModelArray;
import de.obdev.commons.model.ModelElement;
import de.obdev.commons.model.ModelPrimitive;

import java.util.Map;
import java.util.function.Function;

public abstract class BasicUrlDesigner implements UrlDesigner {

  Function<String, String> stringEncoder;

  public BasicUrlDesigner() {
    stringEncoder = s -> Encoders.url().encode(s);
  }

  public abstract String createList(String key, ModelArray array);

  @Override
  public String design(Model input) {
    StringBuilder builder = new StringBuilder();
    builder.append("?");
    for (Map.Entry<String, ModelElement> entry : input.asReal().entrySet()) {
      String key = entry.getKey();
      ModelElement value = entry.getValue();
      if (value instanceof ModelPrimitive) {
        builder.append(key).append("=").append(value.asRaw().toString());
      } else if (value instanceof ModelArray) {
        builder.append(createList(key, (ModelArray) value));
      }

      builder.append("&");
    }
    builder.deleteCharAt(builder.length() - 1);
    return builder.toString();
  }

  boolean packString = false;

  public boolean packString() {
    return packString;
  }

  public BasicUrlDesigner packString(boolean packString) {
    this.packString = packString;
    return this;
  }

  public Function<String, String> stringEncoder() {
    return stringEncoder;
  }

  public BasicUrlDesigner stringEncoder(Function<String, String> stringEncoder) {
    this.stringEncoder = stringEncoder;
    return this;
  }

  public String stringify(ModelElement object) {
    Object raw = object.asRaw();
    if (raw instanceof String && packString)
      return "\"" + stringEncoder.apply(raw.toString()) + "\"";
    return stringEncoder.apply(raw.toString());
  }
}
