package de.obdev.commons.web;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.model.Model;
import de.obdev.commons.web.url.UrlDesigner;
import de.obdev.commons.web.url.implementation.BracketUrlDesigner;

public class HttpGetter {

  String url;

  boolean stringMarks = false;

  Model query = null;

  UrlDesigner designer = new BracketUrlDesigner();

  public HttpGetter designer(UrlDesigner designer) {
    this.designer = designer;
    return this;
  }

  public HttpGetter query(String key, Object value) {
    if (query == null) query = new Model();
    query.add(key, value);
    return this;
  }

  public HttpGetter query(Model model) {
    query = model;
    return this;
  }

  public HttpGetter query(Object pojo) {
    this.query = Encoders.model().encode(pojo);
    return this;
  }
}
