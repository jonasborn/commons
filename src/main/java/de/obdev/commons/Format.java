package de.obdev.commons;

import java.util.Arrays;
import java.util.List;

public enum Format {
  PNG(Arrays.asList("png"), Arrays.asList("image/png")),
  JPEG(Arrays.asList("jpeg", "jpg"), Arrays.asList("image/jpeg")),
  GIF(Arrays.asList("gif"), Arrays.asList("image/gif"));

  static {
    //TODO Show alert if list is <1
  }

  List<String> extensions;
  List<String> mimes;

  Format(List<String> extensions, List<String> mimes) {
    this.extensions = extensions;
    this.mimes = mimes;
  }

  public String getExtension() {
    return extensions.get(0);
  }

  public String getMime() {
    return mimes.get(0);
  }

  public List<String> getExtensions() {
    return extensions;
  }

  public List<String> getMimes() {
    return mimes;
  }
}
