package de.obdev.commons;

import info.debatty.java.stringsimilarity.Damerau;
import info.debatty.java.stringsimilarity.Levenshtein;
import info.debatty.java.stringsimilarity.LongestCommonSubsequence;

public class Strings {

  static Levenshtein levenshtein = new Levenshtein();

  static Damerau damerau = new Damerau();

  static LongestCommonSubsequence longestCommonSubsequence = new LongestCommonSubsequence();

  public static double levenstein(String s1, String s2) {
    return levenshtein.distance(s1, s2);
  }

  public static double damerau(String s1, String s2) {
    return damerau.distance(s1, s2);
  }

  public static double longesCommonSubsequence(String s1, String s2) {
    return longestCommonSubsequence.distance(s1, s2);
  }
}
