package de.obdev.commons.digest;

import de.obdev.commons.convert.Convertable;

import java.security.MessageDigest;

public abstract class AbstractBCDigest extends AbstractDigest {

  MessageDigest digest;

  public AbstractBCDigest(MessageDigest digest) {
    this.digest = digest;
  }

  @Override
  public Convertable handle(Convertable convertable) {
    digest.update(convertable.getBytes());
    return Convertable.of(digest.digest());
  }
}
