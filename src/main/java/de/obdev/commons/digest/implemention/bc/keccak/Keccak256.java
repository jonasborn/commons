package de.obdev.commons.digest.implemention.bc.keccak;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Keccak;

public class Keccak256 extends AbstractBCDigest {
  public Keccak256() {
    super(new Keccak.Digest256());
  }
}
