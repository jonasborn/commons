package de.obdev.commons.digest.implemention.bc.blake2s;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Blake2s;

public class Blake2s244 extends AbstractBCDigest {
  public Blake2s244() {
    super(new Blake2s.Blake2s224());
  }
}
