package de.obdev.commons.digest.implemention.bc.keccak;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Keccak;

public class Keccak384 extends AbstractBCDigest {
  public Keccak384() {
    super(new Keccak.Digest384());
  }
}
