package de.obdev.commons.digest.implemention;

import de.obdev.commons.convert.Convertable;
import de.obdev.commons.digest.AbstractDigest;
import net.openhft.hashing.LongHashFunction;

public class City1_1_64 extends AbstractDigest {

  LongHashFunction hashFunction = LongHashFunction.city_1_1();

  @Override
  public Convertable handle(Convertable convertable) {
    return Convertable.of(hashFunction.hashBytes(convertable.getBytes()));
  }
}
