package de.obdev.commons.digest.implemention.bc.keccak;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Keccak;

public class Keccak512 extends AbstractBCDigest {
  public Keccak512() {
    super(new Keccak.Digest512());
  }
}
