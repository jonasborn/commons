package de.obdev.commons.digest.implemention;

import de.obdev.commons.convert.Convertable;
import de.obdev.commons.digest.AbstractDigest;
import net.openhft.hashing.LongHashFunction;

public class MurMur3_64 extends AbstractDigest {

  LongHashFunction hashFunction = LongHashFunction.murmur_3();

  @Override
  public Convertable handle(Convertable convertable) {
    return Convertable.of(hashFunction.hashBytes(convertable.getBytes()));
  }
}
