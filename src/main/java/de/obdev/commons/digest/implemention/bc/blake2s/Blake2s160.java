package de.obdev.commons.digest.implemention.bc.blake2s;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Blake2s;

public class Blake2s160 extends AbstractBCDigest {
  public Blake2s160() {
    super(new Blake2s.Blake2s160());
  }
}
