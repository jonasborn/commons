package de.obdev.commons.digest.implemention.bc.keccak;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Keccak;

public class Keccak224 extends AbstractBCDigest {

  public Keccak224() {
    super(new Keccak.Digest224());
  }
}
