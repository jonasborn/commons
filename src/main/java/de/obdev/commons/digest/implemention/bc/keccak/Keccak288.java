package de.obdev.commons.digest.implemention.bc.keccak;

import de.obdev.commons.digest.AbstractBCDigest;
import org.bouncycastle.jcajce.provider.digest.Keccak;

public class Keccak288 extends AbstractBCDigest {
  public Keccak288() {
    super(new Keccak.Digest288());
  }
}
