package de.obdev.commons.digest;

import de.obdev.commons.digest.implemention.City1_1_64;
import de.obdev.commons.digest.implemention.MurMur3_64;
import de.obdev.commons.digest.implemention.bc.blake2s.Blake2s128;
import de.obdev.commons.digest.implemention.bc.blake2s.Blake2s160;
import de.obdev.commons.digest.implemention.bc.blake2s.Blake2s244;
import de.obdev.commons.digest.implemention.bc.blake2s.Blake2s265;
import de.obdev.commons.digest.implemention.bc.keccak.*;

public class Digests {

  public static MurMur3_64 murMur3() {
    return new MurMur3_64();
  }

  public static City1_1_64 city1_1() {
    return new City1_1_64();
  }

  public static Blake2s128 blake2s128() {
    return new Blake2s128();
  }

  public static Blake2s160 blake2s160() {
    return new Blake2s160();
  }

  public static Blake2s244 blake2s244() {
    return new Blake2s244();
  }

  public static Blake2s265 blake2s265() {
    return new Blake2s265();
  }

  public static Keccak224 keccak224() {
    return new Keccak224();
  }

  public static Keccak256 keccak256() {
    return new Keccak256();
  }

  public static Keccak288 keccak288() {
    return new Keccak288();
  }

  public static Keccak384 keccak384() {
    return new Keccak384();
  }

  public static Keccak512 keccak512() {
    return new Keccak512();
  }
}
