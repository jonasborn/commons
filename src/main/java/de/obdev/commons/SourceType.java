package de.obdev.commons;

public enum SourceType {
  FILE,
  INTERNAL,
  GET,
  SOCKET
}
