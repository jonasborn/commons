package de.obdev.commons;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class Charsets {

  public static Charset UTF8 = StandardCharsets.UTF_8;

  public static Charset UTF16 = StandardCharsets.UTF_16;

  public static Charset UTF16LE = StandardCharsets.UTF_16LE;

  public static Charset UTF16BE = StandardCharsets.UTF_16BE;

  public static Charset ASCII = StandardCharsets.US_ASCII;

  public static Charset ISO = StandardCharsets.ISO_8859_1;

  public static Charset DEFAULT = UTF8;
}
