package de.obdev.commons.model;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.coder.json.JsonEncoder;
import de.obdev.commons.convert.Convertable;
import org.bson.BsonDocument;
import org.bson.BsonDocumentWrapper;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.conversions.Bson;

import java.io.Serializable;
import java.util.*;

public class Model extends ModelElement implements Map<String, Object>, Serializable, Bson {

  transient Map<String, ModelElement> storage = new HashMap<>();

  public Model(Map<String, Object> source) {

    for (Map.Entry<String, Object> entry : source.entrySet()) {
      if (entry.getValue() instanceof ModelElement) {
        storage.put(entry.getKey(), (ModelElement) entry.getValue());
      } else if (ModelPrimitive.isPrimitive(entry.getValue())) {
        storage.put(entry.getKey(), new ModelPrimitive(entry.getValue()));
      } else if (entry.getValue() instanceof Map) {
        storage.put(entry.getKey(), new Model((Map<String, Object>) entry.getValue()));
      } else if (entry.getValue() instanceof List) {
        storage.put(entry.getKey(), new ModelArray((List<Object>) entry.getValue()));
      }
    }
  }

  public Model() {}

  public Model(String key, Object value) {
    add(key, value);
  }

  public Model(String key, Object... objects) {
    add(key, new ModelArray(objects));
  }

  public Model add(String key, Object value) {
    if (value instanceof ModelElement) {
      storage.put(key, (ModelElement) value);
    } else {
      storage.put(key, new ModelPrimitive(value));
    }
    return this;
  }

  public ModelElement get(String key) {
    return storage.get(key);
  }

  public Map<String, Object> asMap() {
    Map<String, Object> target = new TreeMap<>();
    for (Map.Entry<String, ModelElement> e : storage.entrySet()) {
      target.put(e.getKey(), e.getValue().asRaw());
    }
    return target;
  }

  @Override
  public Object asRaw() {
    return asMap();
  }

  public Map<String, ModelElement> asReal() {
    return storage;
  }

  static JsonEncoder defaultEncoder = Encoders.json().create();

  public static JsonEncoder getDefaultEncoder() {
    return defaultEncoder;
  }

  public static void setDefaultEncoder(JsonEncoder defaultEncoder) {
    Model.defaultEncoder = defaultEncoder;
  }

  public Convertable asJson() {
    return defaultEncoder.encode(this);
  }

  @Override
  public int size() {
    return storage.size();
  }

  @Override
  public boolean isEmpty() {
    return storage.isEmpty();
  }

  @Override
  public boolean containsKey(Object key) {
    return storage.containsKey(key);
  }

  @Override
  public boolean containsValue(Object value) {
    return asMap().containsValue(value);
  }

  @Override
  public Object get(Object key) {
    return storage.get(key).asRaw();
  }

  @Override
  public Object put(String key, Object value) {
    this.add(key, value);
    return value;
  }

  @Override
  public Object remove(Object key) {
    return storage.remove(key);
  }

  @Override
  public void putAll(Map<? extends String, ?> m) {
    for (Entry<? extends String, ?> e : m.entrySet()) {
      this.add(e.getKey(), e.getValue());
    }
  }

  @Override
  public void clear() {
    storage.clear();
  }

  @Override
  public Set<String> keySet() {
    return storage.keySet();
  }

  @Override
  public Collection<Object> values() {
    return asMap().values();
  }

  @Override
  public Set<Entry<String, Object>> entrySet() {
    return asMap().entrySet();
  }

  @Override
  public <TDocument> BsonDocument toBsonDocument(
      Class<TDocument> aClass, CodecRegistry codecRegistry) {

    return new BsonDocumentWrapper(this, codecRegistry.get(Document.class));
  }

  //TODO ADD FUNCTIONS FROM MODEL!
}
