package de.obdev.commons.model;

import java.math.BigDecimal;
import java.math.BigInteger;

public abstract class ModelElement {

  public Model asObject() {
    return (Model) this;
  }

  public ModelArray asList() {
    return (ModelArray) this;
  }

  public boolean asBoolean() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public Number asNumber() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public String asString() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public double asDouble() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public float asFloat() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public long asLong() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public int asInt() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public byte asByte() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public char asChar() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public BigDecimal asBigDecimal() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public BigInteger asBigInteger() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public short asShort() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }

  public Object asRaw() {
    throw new UnsupportedOperationException(this.getClass().getSimpleName());
  }
}
