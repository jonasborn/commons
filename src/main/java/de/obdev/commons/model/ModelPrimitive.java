package de.obdev.commons.model;

import de.obdev.commons.convert.Convertable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.text.ParseException;

public class ModelPrimitive extends ModelElement {

  static Class<?>[] PRIMITIVES;

  static {
    PRIMITIVES =
        new Class[] {
          Integer.TYPE,
          Long.TYPE,
          Short.TYPE,
          Float.TYPE,
          Double.TYPE,
          Byte.TYPE,
          Boolean.TYPE,
          Character.TYPE,
          Integer.class,
          Long.class,
          Short.class,
          Float.class,
          Double.class,
          Byte.class,
          Boolean.class,
          Character.class
        };
  }

  Object value;

  ModelPrimitive(Object o) {
    this.value = o;
  }

  public ModelPrimitive(boolean b) {
    this.value = b;
  }

  public ModelPrimitive(String s) {
    this.value = s;
  }

  public ModelPrimitive(Number n) {
    this.value = n;
  }

  public ModelPrimitive(Character c) {
    this.value = c;
  }

  private Convertable parseString() {
    return Convertable.of((String) value);
  }

  @Override
  public Number asNumber() {
    try {
      return (this.value instanceof Number) ? (Number) value : parseString().asNumber();
    } catch (ParseException e) {
      throw new RuntimeException(e);
    }
  }

  @Override
  public BigDecimal asBigDecimal() {
    return (this.value instanceof BigDecimal) ? (BigDecimal) value : parseString().asBigDecimal();
  }

  @Override
  public BigInteger asBigInteger() {
    return (this.value instanceof BigInteger) ? (BigInteger) value : parseString().asBigInteger();
  }

  @Override
  public short asShort() {
    return (this.value instanceof Short) ? (short) value : parseString().asShort();
  }

  @Override
  public int asInt() {
    return (this.value instanceof Integer) ? (int) value : parseString().asInt();
  }

  @Override
  public float asFloat() {
    return (this.value instanceof Float) ? (float) value : parseString().asFloat();
  }

  @Override
  public long asLong() {
    return (this.value instanceof Long) ? (long) value : parseString().asLong();
  }

  @Override
  public byte asByte() {
    return (this.value instanceof Byte) ? (byte) value : parseString().asByte();
  }

  @Override
  public String asString() {
    return value.toString();
  }

  @Override
  public Object asRaw() {
    return value;
  }

  static boolean isPrimitive(Object o) {
    if (o instanceof String) return true;
    for (int i = 0; i < PRIMITIVES.length; i++) {
      Class<?> primitive = PRIMITIVES[i];
      if (primitive.isAssignableFrom(o.getClass())) return true;
    }
    return false;
  }

  static boolean isString(Object target) {
    if (target instanceof String) {
      return true;
    } else {
      Class<?> classOfPrimitive = target.getClass();
      Class[] var2 = PRIMITIVES;
      int var3 = var2.length;

      for (int var4 = 0; var4 < var3; ++var4) {
        Class<?> standardPrimitive = var2[var4];
        if (standardPrimitive.isAssignableFrom(classOfPrimitive)) {
          return true;
        }
      }

      return false;
    }
  }

  public int hashCode() {
    if (this.value == null) {
      return 31;
    } else {
      long value;
      if (isIntegral(this)) {
        value = this.asNumber().longValue();
        return (int) (value ^ value >>> 32);
      } else if (this.value instanceof Number) {
        value = Double.doubleToLongBits(this.asNumber().doubleValue());
        return (int) (value ^ value >>> 32);
      } else {
        return this.value.hashCode();
      }
    }
  }

  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    } else if (obj != null && this.getClass() == obj.getClass()) {
      ModelPrimitive other = (ModelPrimitive) obj;
      if (this.value == null) {
        return other.value == null;
      } else if (isIntegral(this) && isIntegral(other)) {
        return this.asNumber().longValue() == other.asNumber().longValue();
      } else if (this.value instanceof Number && other.value instanceof Number) {
        double a = this.asNumber().doubleValue();
        double b = other.asNumber().doubleValue();
        return a == b || Double.isNaN(a) && Double.isNaN(b);
      } else {
        return this.value.equals(other.value);
      }
    } else {
      return false;
    }
  }

  private static boolean isIntegral(ModelPrimitive primitive) {
    if (!(primitive.value instanceof Number)) {
      return false;
    } else {
      Number number = (Number) primitive.value;
      return number instanceof BigInteger
          || number instanceof Long
          || number instanceof Integer
          || number instanceof Short
          || number instanceof Byte;
    }
  }
}
