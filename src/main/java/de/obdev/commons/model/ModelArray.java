package de.obdev.commons.model;

import java.util.*;
import java.util.function.Consumer;

public class ModelArray extends ModelElement implements Iterable<ModelElement> {

  List<ModelElement> storage = new ArrayList<>();

  public ModelArray(Object... objects) {
    for (int i = 0; i < objects.length; i++) {
      this.add(objects[i]);
    }
  }

  public ModelArray(List<Object> source) {
    for (int i = 0; i < source.size(); i++) {
      this.add(source.get(i));
    }
  }

  public int size() {
    return storage.size();
  }

  @Override
  public Object asRaw() {
    List<Object> target = new ArrayList<>();
    for (ModelElement e : storage) {
      target.add(e.asRaw());
    }
    return target;
  }

  public ModelArray add(Object... objects) {
    for (int i = 0; i < objects.length; i++) {
      add(objects[i]);
    }
    return this;
  }

  public ModelArray add(Object o) {
    if (o instanceof ModelElement) {
      System.out.println("ModelElement");
      storage.add((ModelElement) o);
    } else if (ModelPrimitive.isPrimitive(o)) {
      System.out.println("ModelPrimitive");
      storage.add(new ModelPrimitive(o));
    } else if (o instanceof List) {
      System.out.println("List");
      storage.add(new ModelArray((List<Object>) (o)));
    } else if (o instanceof Map) {
      System.out.println("Map");
      storage.add(new Model((Map<String, Object>) o));
    }
    return this;
  }

  @Override
  public Iterator<ModelElement> iterator() {
    return storage.iterator();
  }

  @Override
  public void forEach(Consumer<? super ModelElement> action) {
    storage.forEach(action);
  }

  @Override
  public Spliterator<ModelElement> spliterator() {
    return storage.spliterator();
  }
}
