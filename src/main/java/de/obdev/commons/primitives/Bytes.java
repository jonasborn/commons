package de.obdev.commons.primitives;

import de.obdev.commons.coder.Decoders;
import de.obdev.commons.coder.Encoders;

public class Bytes {

  private static final char[] HEX_CHARS = "0123456789ABCDEF".toCharArray();

  public static String hex(byte[] bytes) {
    char[] hexChars = new char[bytes.length * 2];
    for (int j = 0; j < bytes.length; j++) {
      int v = bytes[j] & 0xFF;
      hexChars[j * 2] = HEX_CHARS[v >>> 4];
      hexChars[j * 2 + 1] = HEX_CHARS[v & 0x0F];
    }
    return new String(hexChars);
  }

  public static String base64(byte[] bytes) {
    return Encoders.base64().asString(bytes);
  }

  public static byte[] base64(CharSequence base) {
    return Decoders.base64().decode(base);
  }

  public static byte[] cut(byte[] soruce, int offset) {
    byte[] fin = new byte[offset];
    System.arraycopy(soruce, 0, fin, 0, offset);
    return fin;
  }

  public static byte[] copy(byte[] source) {
    byte[] fin = new byte[source.length];
    System.arraycopy(source, 0, fin, 0, source.length);
    return fin;
  }

}
