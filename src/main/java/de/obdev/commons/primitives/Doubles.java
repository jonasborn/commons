package de.obdev.commons.primitives;

import de.obdev.commons.convert.Convertable;

public class Doubles {

  public double parse(String s) {
    return Convertable.of(s).asDouble();
  }

  public double parse(byte[] bytes) {
    return Convertable.of(bytes).asDouble();
  }
}
