package de.obdev.commons.primitives;

import de.obdev.commons.convert.Convertable;

public class Ints {

  public int parse(String s) {
    return Convertable.of(s).asInt();
  }
}
