package de.obdev.commons.primitives;

import de.obdev.commons.convert.Convertable;

public class Shorts {

  public static byte[] asBytes(short s) {
    return Convertable.of(s).asBytes();
  }
}
