package de.obdev.commons.crypto;

import de.obdev.commons.crypto.asymmetric.AsymmetricKeyPairs;
import de.obdev.commons.crypto.asymmetric.AsymmetricKeys;
import de.obdev.commons.crypto.symmetric.SymmetricKeys;

public class Keys {

  public static SymmetricKeys symmetric() {
    return new SymmetricKeys();
  }

  public static AsymmetricKeys asymmetric() {
    return new AsymmetricKeys();
  }

  public static AsymmetricKeyPairs asymmetricPairs() {
    return new AsymmetricKeyPairs();
  }
}
