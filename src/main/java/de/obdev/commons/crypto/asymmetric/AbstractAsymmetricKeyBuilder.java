package de.obdev.commons.crypto.asymmetric;

public abstract class AbstractAsymmetricKeyBuilder {

  public abstract AsymmetricKeyPair create() throws Exception;
}
