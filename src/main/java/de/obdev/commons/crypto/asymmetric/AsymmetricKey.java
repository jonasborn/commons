package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.coder.Encoders;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class AsymmetricKey {

  byte[] encoded;
  KeyType type;

  public AsymmetricKey(KeyType type, byte[] encoded) {
    this.type = type;
    this.encoded = encoded;
  }

  public KeyType getType() {
    return type;
  }

  public byte[] getEncoded() {
    return encoded;
  }

  public byte[] asBytes() throws IOException {
    ByteArrayOutputStream bout = new ByteArrayOutputStream(encoded.length + 1);
    bout.write(type.asByte());
    bout.write(encoded);
    return bout.toByteArray();
    //return getEncoded();
  }

  public String asBase64() throws IOException {
    return Encoders.base64().asString(asBytes());
  }
}
