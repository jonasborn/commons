package de.obdev.commons.crypto.asymmetric;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;

public abstract class AbstractAsymmetricCipher {

  public abstract KeyFactory keyFactory() throws Exception;

  public abstract KeySpec privateKeySpec(AsymmetricKey key);

  public abstract KeySpec publicKeySpec(AsymmetricKey key);

  public AlgorithmParameterSpec parameterSpec() {
    return null;
  }

  public PublicKey generatePublic(AsymmetricKey key) throws Exception {
    KeySpec spec = publicKeySpec(key);
    KeyFactory factory = keyFactory();
    return factory.generatePublic(spec);
  }

  public PrivateKey generatePrivate(AsymmetricKey key) throws Exception {
    KeySpec spec = privateKeySpec(key);
    KeyFactory factory = keyFactory();
    return factory.generatePrivate(spec);
  }

  Cipher encrypt = null;
  Cipher decrypt = null;

  String cipher;
  String provider;

  Key key;

  AlgorithmParameterSpec spec;

  public AbstractAsymmetricCipher(String cipher, String provider, AsymmetricKey key)
      throws Exception {
    this.cipher = cipher;
    this.provider = provider;

    KeyType type = key.getType();
    System.out.println(type);
    if (type == KeyType.SECRET) {
      this.key = generatePrivate(key);
    } else if (type == KeyType.SHARED) {
      this.key = generatePublic(key);
    }

    spec = parameterSpec();
  }

  private Cipher createCipher(Integer mode, Key key) throws Exception {
    Cipher c = Cipher.getInstance(cipher, provider);
    if (spec != null) {
      c.init(mode, key, spec);
    } else {
      c.init(mode, key);
    }
    return c;
  }

  public byte[] encrypt(byte[] input) throws Exception {
    encrypt = createCipher(Cipher.ENCRYPT_MODE, key);
    System.out.println(input.length);
    return encrypt.doFinal(input);
  }

  public byte[] decrypt(byte[] input) throws Exception {
    decrypt = createCipher(Cipher.DECRYPT_MODE, key);
    return decrypt.doFinal(input);
  }
}
