package de.obdev.commons.crypto.asymmetric.implemention;

import de.obdev.commons.crypto.asymmetric.AbstractSigner;
import de.obdev.commons.crypto.asymmetric.AsymmetricKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.KeyFactory;
import java.security.Security;
import java.security.Signature;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSASigner extends AbstractSigner {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public RSASigner(AsymmetricKey key) throws Exception {
        super(key);
    }

    @Override
    public Signature signature() throws Exception {
        return Signature.getInstance("SHA1withRSA", "BC");
    }

    @Override
    public KeyFactory keyFactory() throws Exception {
        return KeyFactory.getInstance("RSA", "BC");
    }

    @Override
    public KeySpec privateKeySpec(AsymmetricKey key) {
        return new PKCS8EncodedKeySpec(key.getEncoded());
    }

    @Override
    public KeySpec publicKeySpec(AsymmetricKey key) {
        return new X509EncodedKeySpec(key.getEncoded());
    }
}
