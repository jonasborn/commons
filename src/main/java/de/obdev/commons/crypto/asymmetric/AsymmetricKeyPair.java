package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.coder.Encoders;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.KeyPair;

public class AsymmetricKeyPair {

  AsymmetricKey secret;
  AsymmetricKey shared;

  public AsymmetricKeyPair(KeyPair keyPair) {
    secret = new AsymmetricKey(KeyType.SECRET, keyPair.getPrivate().getEncoded());
    shared = new AsymmetricKey(KeyType.SHARED, keyPair.getPublic().getEncoded());
  }

  public AsymmetricKeyPair(AsymmetricKey secret, AsymmetricKey shared) {
    this.secret = secret;
    this.shared = shared;
  }

  public AsymmetricKey getSecret() {
    return secret;
  }

  public AsymmetricKey getShared() {
    return shared;
  }

  public byte[] asBytes() throws IOException {
    byte[] secretBytes = secret.getEncoded();
    byte[] sharedBytes = shared.getEncoded();
    ByteArrayOutputStream bout =
        new ByteArrayOutputStream(4 + sharedBytes.length + secretBytes.length);
    bout.write(sharedBytes.length);
    bout.write(sharedBytes);
    bout.write(secretBytes);
    return bout.toByteArray();
  }

  public String asBase64() throws IOException {
    return Encoders.base64().asString(asBytes());
  }
}
