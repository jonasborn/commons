package de.obdev.commons.crypto.asymmetric;

import java.nio.ByteBuffer;

public class AsymmetricKeys {

  public AsymmetricKey parse(byte[] bytes) {
    ByteBuffer buffer = ByteBuffer.wrap(bytes);
    KeyType type = KeyType.parse(buffer.get());
    byte[] res = new byte[buffer.remaining()];
    buffer.get(res);
    return new AsymmetricKey(type, res);
  }

  //TODO ADD PARSER!

}
