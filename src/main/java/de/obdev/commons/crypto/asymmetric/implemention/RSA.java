package de.obdev.commons.crypto.asymmetric.implemention;

import de.obdev.commons.crypto.asymmetric.AbstractAsymmetricCipher;
import de.obdev.commons.crypto.asymmetric.AsymmetricKey;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.KeyGenerator;
import java.security.KeyFactory;
import java.security.Security;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class RSA extends AbstractAsymmetricCipher {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    public RSA(AsymmetricKey key) throws Exception {
        super("RSA", "BC", key);
    }

    @Override
    public KeyFactory keyFactory() throws Exception {
        return KeyFactory.getInstance("RSA", "BC");
    }

    @Override
    public KeySpec privateKeySpec(AsymmetricKey key) {
        return new PKCS8EncodedKeySpec(key.getEncoded());
    }

    @Override
    public KeySpec publicKeySpec(AsymmetricKey key) {
        return new X509EncodedKeySpec(key.getEncoded());
    }
}
