package de.obdev.commons.crypto.asymmetric.implemention;

import de.flexiprovider.core.FlexiCoreProvider;
import de.flexiprovider.ec.FlexiECProvider;
import de.obdev.commons.crypto.asymmetric.AbstractSigner;
import de.obdev.commons.crypto.asymmetric.AsymmetricKey;

import java.security.KeyFactory;
import java.security.Security;
import java.security.Signature;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class McElieceSigner extends AbstractSigner {

  static {
    Security.addProvider(new FlexiCoreProvider());
    Security.addProvider(new FlexiECProvider());
  }

  public McElieceSigner(AsymmetricKey key) throws Exception {
    super(key);
  }

  @Override
  public Signature signature() throws Exception {
    return Signature.getInstance("SHA1withECDSA", "FlexiEC");
  }

  @Override
  public KeyFactory keyFactory() throws Exception {
    return KeyFactory.getInstance("ECIES", "FlexiEC");
  }

  @Override
  public KeySpec privateKeySpec(AsymmetricKey key) {
    return new PKCS8EncodedKeySpec(key.getEncoded());
  }

  @Override
  public KeySpec publicKeySpec(AsymmetricKey key) {
    return new X509EncodedKeySpec(key.getEncoded());
  }
}
