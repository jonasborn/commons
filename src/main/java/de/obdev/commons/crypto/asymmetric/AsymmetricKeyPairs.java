package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.crypto.asymmetric.implemention.McElieceKeyBuilder;
import de.obdev.commons.crypto.asymmetric.implemention.RSAKeyBuilder;

import java.nio.ByteBuffer;

public class AsymmetricKeyPairs {

  public McElieceKeyBuilder mcEliece() {
    return new McElieceKeyBuilder();
  }

  public RSAKeyBuilder rsa() {
    return new RSAKeyBuilder();
  }

  public AsymmetricKeyPair parse(byte[] bytes) {
    ByteBuffer buffer = ByteBuffer.wrap(bytes);
    int sharedLength = buffer.getInt();
    byte[] shared = new byte[sharedLength];
    buffer.get(shared);
    byte[] secret = new byte[buffer.remaining()];
    buffer.get(secret);
    AsymmetricKey secretKey = new AsymmetricKey(KeyType.SECRET, secret);
    AsymmetricKey sharedKey = new AsymmetricKey(KeyType.SHARED, shared);
    return new AsymmetricKeyPair(secretKey, sharedKey);
  }
}
