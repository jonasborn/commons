package de.obdev.commons.crypto.asymmetric.implemention;

import de.obdev.commons.crypto.asymmetric.AbstractAsymmetricKeyBuilder;
import de.obdev.commons.crypto.asymmetric.AsymmetricKeyPair;
import org.bouncycastle.jce.provider.BouncyCastleProvider;

import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.Security;

public class RSAKeyBuilder extends AbstractAsymmetricKeyBuilder {

    static {
        Security.addProvider(new BouncyCastleProvider());
    }

    int size = 128;

    public int size() {
        return size;
    }

    public RSAKeyBuilder size(int size) {
        this.size = size;
        return this;
    }

    @Override
    public AsymmetricKeyPair create() throws Exception {
        KeyPairGenerator kpg = KeyPairGenerator.getInstance("RSA", "BC");
        kpg.initialize(size);
        KeyPair keyPair = kpg.generateKeyPair();
        return new AsymmetricKeyPair(keyPair);
    }
}
