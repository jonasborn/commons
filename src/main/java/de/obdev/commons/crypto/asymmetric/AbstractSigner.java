package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.crypto.Signed;

import java.security.*;
import java.security.spec.KeySpec;

public abstract class AbstractSigner {

  public abstract Signature signature() throws Exception;

  public abstract KeyFactory keyFactory() throws Exception;

  public abstract KeySpec privateKeySpec(AsymmetricKey key);

  public abstract KeySpec publicKeySpec(AsymmetricKey key);

  private PrivateKey generatePrivate(AsymmetricKey key) throws Exception {
    KeySpec spec = privateKeySpec(key);
    KeyFactory factory = keyFactory();
    return factory.generatePrivate(spec);
  }

  private PublicKey generatePublic(AsymmetricKey key) throws Exception {
    KeySpec spec = publicKeySpec(key);
    KeyFactory factory = keyFactory();
    return factory.generatePublic(spec);
  }

  private Key internalKey;
  private AsymmetricKey key;

  public AbstractSigner(AsymmetricKey key) throws Exception {
    this.key = key;
    switch (key.type) {
      case SECRET:
        this.internalKey = generatePrivate(key);
        break;
      case SHARED:
        this.internalKey = generatePublic(key);
        break;
    }
  }

  public boolean verify(byte[] message, byte[] input) {
    try {
      Signature signature = signature();
      signature.initVerify((PublicKey) internalKey);
      signature.update(message);
      return signature.verify(input);
    } catch (Exception e) {
      return false;
    }
  }

  public byte[] asBytes(byte[] data) throws Exception {
    Signature signature = signature();
    signature.initSign((PrivateKey) internalKey);
    signature.update(data);
    return signature.sign();
  }

  public String asBase64(byte[] data) throws Exception {
    return Encoders.base64().asString(asBytes(data));
  }

  public Signed asSigned(byte[] data, AsymmetricKey shared) throws Exception {
    Signed signed = new Signed(data);
    signed.add(this, shared);
    return signed;
  }

  public AsymmetricKey getKey() {
    return key;
  }
}
