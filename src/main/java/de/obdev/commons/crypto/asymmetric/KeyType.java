package de.obdev.commons.crypto.asymmetric;

public enum KeyType {
  SECRET(0),
  SHARED(1);

  int i;

  KeyType(int i) {
    this.i = i;
  }

  public int asInt() {
    return i;
  }

  public byte asByte() {
    return (byte) i;
  }

  public static KeyType parse(int i) {
    if (i == 0) return SECRET;
    else return SHARED;
  }

  public static KeyType parse(byte b) {
    int i = (int) b;
    return parse(i);
  }
}
