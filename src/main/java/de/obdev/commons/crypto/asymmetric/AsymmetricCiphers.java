package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.crypto.asymmetric.implemention.McEliece;
import de.obdev.commons.crypto.asymmetric.implemention.RSA;

public class AsymmetricCiphers {

  public McEliece mcEliece(AsymmetricKey key) throws Exception {
    return new McEliece(key);
  }

  public RSA rsa(AsymmetricKey key) throws Exception {
    return new RSA(key);
  }
}
