package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.crypto.asymmetric.implemention.McElieceSigner;
import de.obdev.commons.crypto.asymmetric.implemention.RSASigner;

public class AsymmetricSigners {

  public McElieceSigner mcEliece(AsymmetricKey key) throws Exception {
    return new McElieceSigner(key);
  }

  public RSASigner rsa(AsymmetricKey key) throws Exception {
    return new RSASigner(key);
  }
}
