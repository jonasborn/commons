package de.obdev.commons.crypto.asymmetric.implemention;

import de.flexiprovider.common.ies.IESParameterSpec;
import de.flexiprovider.core.FlexiCoreProvider;
import de.flexiprovider.pqc.FlexiPQCProvider;
import de.obdev.commons.crypto.asymmetric.AbstractAsymmetricCipher;
import de.obdev.commons.crypto.asymmetric.AsymmetricKey;

import java.security.KeyFactory;
import java.security.Security;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.KeySpec;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;

public class McEliece extends AbstractAsymmetricCipher {

  static {
    Security.addProvider(new FlexiCoreProvider());
    Security.addProvider(new FlexiPQCProvider());
  }

  public McEliece(AsymmetricKey key) throws Exception {
    super("ECIES", "FlexiEC", key);
  }

  @Override
  public KeyFactory keyFactory() throws Exception {
    return KeyFactory.getInstance("ECIES", "FlexiEC");
  }

  @Override
  public KeySpec privateKeySpec(AsymmetricKey key) {
    return new PKCS8EncodedKeySpec(key.getEncoded());
  }

  @Override
  public KeySpec publicKeySpec(AsymmetricKey key) {
    return new X509EncodedKeySpec(key.getEncoded());
  }

  @Override
  public AlgorithmParameterSpec parameterSpec() {
    //return new IESParameterSpec("AES128-CBC", "HmacSHA1", null, null);
    return null;
  }
}
