package de.obdev.commons.crypto.asymmetric.implemention;

import de.flexiprovider.core.FlexiCoreProvider;
import de.flexiprovider.ec.FlexiECProvider;
import de.flexiprovider.pqc.FlexiPQCProvider;
import de.obdev.commons.crypto.asymmetric.AbstractAsymmetricKeyBuilder;
import de.obdev.commons.crypto.asymmetric.AsymmetricKeyPair;

import java.security.*;

public class McElieceKeyBuilder extends AbstractAsymmetricKeyBuilder {

  static {
    Security.addProvider(new FlexiCoreProvider());
    Security.addProvider(new FlexiECProvider());
  }

  int size = 128;

  public int size() {
    return size;
  }

  public McElieceKeyBuilder size(int size) {
    this.size = size;
    return this;
  }

  public AsymmetricKeyPair create() throws NoSuchProviderException, NoSuchAlgorithmException {
    KeyPairGenerator kpg = KeyPairGenerator.getInstance("ECIES", "FlexiEC");
    kpg.initialize(size);
    KeyPair keyPair = kpg.generateKeyPair();
    return new AsymmetricKeyPair(keyPair);
  }
}
