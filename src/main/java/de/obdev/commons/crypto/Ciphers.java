package de.obdev.commons.crypto;

import de.obdev.commons.crypto.asymmetric.AsymmetricCiphers;
import de.obdev.commons.crypto.symmetric.SymmetricCiphers;

public class Ciphers {

  public static SymmetricCiphers symmetric() {
    return new SymmetricCiphers();
  }

  public static AsymmetricCiphers asymmetric() {
    return new AsymmetricCiphers();
  }
}
