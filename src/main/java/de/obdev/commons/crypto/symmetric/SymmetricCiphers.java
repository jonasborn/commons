package de.obdev.commons.crypto.symmetric;

import de.obdev.commons.crypto.symmetric.implementation.AESCipher;
import de.obdev.commons.crypto.symmetric.implementation.TwoFishCipher;

public class SymmetricCiphers {

  public AESCipher aes(SymmetricKey key) throws Exception {
    return new AESCipher(key);
  }

  public TwoFishCipher twofish(SymmetricKey key) throws Exception {
    return new TwoFishCipher(key);
  }
}
