package de.obdev.commons.crypto.symmetric.implementation;

import de.obdev.commons.crypto.symmetric.SymmetricCipher;
import de.obdev.commons.crypto.symmetric.SymmetricKey;

public class TwoFishCipher extends SymmetricCipher {
  public TwoFishCipher(SymmetricKey key) throws Exception {
    super(key, "twofish", "twofish");
  }
}
