package de.obdev.commons.crypto.symmetric;

import de.obdev.commons.coder.Decoders;

import java.nio.ByteBuffer;

public class SymmetricKeys {

  public SymmetricKey parse(String base64) {
    byte[] res = Decoders.base64().decode(base64);
    return parse(res);
  }

  public SymmetricKey parse(byte[] bytes) {
    ByteBuffer buffer = ByteBuffer.wrap(bytes);
    int ivLength = (int) buffer.get();
    byte[] ivs = new byte[ivLength];
    buffer.get(ivs);
    byte[] secret = new byte[buffer.remaining()];
    buffer.get(secret);
    return new SymmetricKey(secret, ivs);
  }

  public SymmetricKey generate() {
    return new SymmetricKey();
  }
}
