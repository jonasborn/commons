package de.obdev.commons.crypto.symmetric.implementation;

import de.obdev.commons.crypto.symmetric.SymmetricCipher;
import de.obdev.commons.crypto.symmetric.SymmetricKey;

public class AESCipher extends SymmetricCipher {

  public AESCipher(SymmetricKey key) throws Exception {
    super(key, "AES/CBC/PKCS5Padding", "AES");
  }
}
