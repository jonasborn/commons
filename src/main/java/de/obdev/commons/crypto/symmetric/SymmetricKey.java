package de.obdev.commons.crypto.symmetric;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.random.Randoms;

import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class SymmetricKey {

  byte[] secret;
  byte[] ivs;

  public SymmetricKey(byte[] secret, byte[] ivs) {
    this.secret = secret;
    this.ivs = ivs;
  }

  public SymmetricKey() {
    this.secret = Randoms.secure().getBytes(32); //TODO CHECK AND FIX!
    this.ivs = Randoms.secure().getBytes(16); //TODO CHECK AND FIX!
  }

  public byte[] asBytes() throws IOException {
    byte ivLength = (byte) ivs.length;
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    bout.write(ivLength);
    bout.write(ivs);
    bout.write(secret);
    return bout.toByteArray();
  }

  public String asString() throws IOException {
    byte[] res = asBytes();
    return Encoders.base64().asString(res);
  }

  public byte[] getSecret() {
    return secret;
  }

  public byte[] getIvs() {
    return ivs;
  }

  public SecretKeySpec getKeySpec(String cipher) {
    return new SecretKeySpec(getSecret(), cipher);
  }
}
