package de.obdev.commons.crypto.symmetric;

import org.bouncycastle.jce.provider.BouncyCastleProvider;

import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.security.spec.InvalidKeySpecException;

public class SymmetricCipher {

  static {
    Security.addProvider(new BouncyCastleProvider());
  }

  String cipher;
  String keyType;
  SymmetricKey key;

  Cipher encode;
  Cipher decode;

  public SymmetricCipher(SymmetricKey key, String cipher, String keyType) throws Exception {
    this.cipher = cipher;
    this.key = key;
    this.keyType = keyType;
    this.encode = createCipher(Cipher.ENCRYPT_MODE);
    this.decode = createCipher(Cipher.DECRYPT_MODE);
  }

  public Cipher createCipher(Integer mode)
      throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeySpecException,
          InvalidAlgorithmParameterException, InvalidKeyException, UnsupportedEncodingException {
    Cipher cipher = Cipher.getInstance(this.cipher);
    cipher.init(mode, key.getKeySpec(this.keyType), new IvParameterSpec(key.getIvs()));
    return cipher;
  }

  public byte[] encrypt(byte[] input) throws Exception {
    return encode.doFinal(input);
  }

  public byte[] decrypt(byte[] input) throws Exception {
    return decode.doFinal(input);
  }
}
