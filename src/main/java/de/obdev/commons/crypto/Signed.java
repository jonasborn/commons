package de.obdev.commons.crypto;

import de.obdev.commons.Charsets;
import de.obdev.commons.crypto.asymmetric.AbstractSigner;
import de.obdev.commons.crypto.asymmetric.AsymmetricKey;
import de.obdev.commons.digest.Digests;
import de.obdev.commons.primitives.Bytes;
import de.obdev.commons.primitives.Shorts;
import de.obdev.commons.util.Byteable;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

public class Signed implements Byteable<Signed> {

  byte[] body;
  Map<String, byte[]> signatures = new HashMap<>();

  public Signed(Byteable body) throws IOException {
    this.body = body.asBytes();
  }

  public Signed(byte[] body) {
    this.body = body;
  }

  public Signed() {}

  public byte[] asBytes() throws IOException {
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    bout.write(Shorts.asBytes((short) signatures.size()));
    for (Map.Entry<String, byte[]> entry : signatures.entrySet()) {
      byte[] key = entry.getKey().getBytes(Charsets.ASCII);
      short hashLength = (short) key.length;
      bout.write(Shorts.asBytes(hashLength));
      bout.write(key);

      short sigLength = (short) entry.getValue().length;
      bout.write(Shorts.asBytes(sigLength));
      bout.write(entry.getValue());
    }
    bout.write(body);
    return bout.toByteArray();
  }

  public String hash(AsymmetricKey key) {
    return Bytes.hex(Digests.murMur3().getBytes(key.getEncoded()));
  }

  public void add(AbstractSigner signer, AsymmetricKey shared) throws Exception {
    byte[] sig = signer.asBytes(body);
    String hash = hash(shared);
    if (!this.signatures.containsKey(hash)) this.signatures.put(hash, sig);
  }

  public boolean verify(AbstractSigner signer) {
    String hash = hash(signer.getKey());
    if (!signatures.containsKey(hash)) return false;
    byte[] signature = signatures.get(hash);
    return signer.verify(body, signature);
  }

  @Override
  public Signed fromBytes(byte[] bytes) {
    ByteBuffer buffer = ByteBuffer.wrap(bytes);
    short size = buffer.getShort();
    Map<String, byte[]> res = new HashMap<>();
    for (int i = 0; i < size; i++) {

      short keyLength = buffer.getShort();
      byte[] key = new byte[keyLength];
      buffer.get(key);
      System.out.println("+++" + new String(key, Charsets.ASCII));
      short valueLength = buffer.getShort();
      byte[] value = new byte[valueLength];
      buffer.get(value);

      res.put(new String(key, Charsets.ASCII), value);
    }
    byte[] body = new byte[buffer.remaining()];
    buffer.get(body);
    this.body = body;

    this.signatures = res;
    return this;
  }
}
