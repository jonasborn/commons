package de.obdev.commons.starter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.reflections.Reflections;

import java.util.*;

public class Starter {

  static Logger logger = LogManager.getLogger();

  private Starter() {}

  public static void start(String... bases) {
    for (int i = 0; i < bases.length; i++) {
      start(bases[i]);
    }
  }

  public static void start(String base) {
    Reflections reflections = new Reflections(base, new Scanner[0]);
    Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Startable.class);
    List<String> loaded = new ArrayList();

    label51:
    for (int i = 0; i < annotated.size() * 5; ++i) {
      Iterator var4 = annotated.iterator();

      while (true) {
        Class cls;
        do {
          if (!var4.hasNext()) {
            continue label51;
          }

          cls = (Class) var4.next();
        } while (loaded.contains(cls.getName()));

        List<String> requirements = new ArrayList();
        Startable loadable = (Startable) cls.getAnnotation(Startable.class);
        if (loadable != null && loadable.value().length > 0) {
          Class[] var8 = loadable.value();
          int var9 = var8.length;

          for (int var10 = 0; var10 < var9; ++var10) {
            Class req = var8[var10];
            if (!req.isAnnotationPresent(Startable.class)) {
              logger.warn(
                  "{} is waiting for a not annotated class ({})", cls.getName(), req.getName());
            }

            requirements.add(req.getName());
          }
        }

        if (loaded.containsAll(requirements)) {
          try {
            Class.forName(cls.getName());
            loaded.add(cls.getName());
            logger.debug("Loaded {}", cls.getName());
          } catch (ClassNotFoundException | ExceptionInInitializerError var12) {
            logger.warn("Unable to load {}: {}", cls.getName(), var12);
            var12.printStackTrace();
          }
        }
      }
    }

    logger.info("Loaded {} of {}", loaded.size(), annotated.size());
  }
}
