package de.obdev.commons;

import java.util.function.Consumer;

public class ThrowableResult {

  Throwable throwable = null;

  public ThrowableResult() {}

  public ThrowableResult(Throwable throwable) {
    this.throwable = throwable;
  }

  public void handle(Consumer<Throwable> t) {
    if (throwable != null) t.accept(throwable);
  }

  public boolean worked() {
    return throwable == null;
  }
}
