package de.obdev.commons.io;

import de.obdev.commons.async.Async;
import de.obdev.commons.async.AsyncResult;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Files {

  static String temp = System.getProperty("java.io.tmpdir");

  static Logger logger = LogManager.getLogger();

  public static class TempBuilder {

    static Callable<String[]> rand;

    static {
      if (!new File(temp).canWrite()) {
        logger.fatal("Unable to write in temp directory ({})", temp);
        System.exit(1);
      }

      rand =
          () -> {
            String name = UUID.randomUUID().toString();
            return new String[] {name.substring(0, 2), name};
          };
    }

    private String ext = ".tmp";

    private boolean create = true;
    private boolean clean = true;

    public TempBuilder ext(String ext) {
      this.ext = ext;
      return this;
    }

    public TempBuilder create(boolean status) {
      this.create = status;
      return this;
    }

    public TempBuilder clean(boolean status) {
      this.clean = status;
      return this;
    }

    private String prepare(String ext) {
      String[] path;
      try {
        path = rand.call();
      } catch (Exception e) {
        return UUID.randomUUID().toString() + ext;
      }
      String name = path[path.length - 1];
      path[path.length - 1] = "";
      File base = new File(tempDirectory(), String.join("/", path));
      base.mkdirs();
      return base + "/" + name + ext;
    }

    public File file() {
      File file = new File(prepare(ext));
      file.delete();
      try {
        if (create) file.createNewFile();
        if (clean) file.deleteOnExit();
      } catch (IOException e) {
        logger.fatal("Unable to write {} as file in temp directory ({}): {}", file, temp, e);
      }

      return file;
    }

    public File folder() {
      File file = new File(tempDirectory(), prepare(ext));
      File folder = new File(tempDirectory(), file.getPath());
      try {
        boolean created = folder.mkdirs();
        if (!created) logger.fatal("Unable to create temp directory {}", folder);
      } catch (Exception e) {
        logger.fatal("Unable to create temp directory {}: {}", folder, e);
      }
      return folder;
    }
  }

  public static TempBuilder temp() {
    return new TempBuilder();
  }

  public static File write(byte[] source, File target) throws Exception {
    Streams.to(target).write(source).await();
    return target;
  }

  public static File write(byte[] source) throws Exception {
    File target = Files.temp().file();
    write(source, target);
    return target;
  }

  public static File write(String source, File target) throws Exception {
    byte[] data;
    try {
      data = source.getBytes("UTF-8");
    } catch (UnsupportedEncodingException e) {
      data = source.getBytes();
    }
    return write(data, target);
  }

  public static File write(String source) throws Exception {
    File target = Files.temp().file();
    write(source, target);
    return target;
  }

  public static File write(List<String> source, File target) throws IOException {
    BufferedWriter writer = new BufferedWriter(new FileWriter(target));
    source.forEach(
        e -> {
          try {
            writer.write(e);
          } catch (IOException e1) {
            logger.fatal("Unable to write file {}: {}", target, e);
          }
        });
    return target;
  }

  public static File write(List<String> source) throws IOException {
    File target = Files.temp().file();
    write(source, target);
    return target;
  }

  public static InputBuilder read(File file) {
    return new InputBuilder(file);
  }

  public static class InputBuilder {

    File file;

    public InputBuilder(File file) {
      this.file = file;
    }

    public byte[] bytes() throws Exception {
      AsyncResult<byte[]> result = Streams.from(file).bytes().await();
      if (result.isPresent()) {
        return result.get();
      } else {
        throw new Exception("Unable to decode file");
      }
    }

    public List<String> lines() throws IOException {
      List<String> target = new ArrayList<>();
      try (BufferedReader br = new BufferedReader(new FileReader(file))) {
        for (String line; (line = br.readLine()) != null; ) {
          target.add(line);
        }
      }
      return target;
    }

    public String string() throws Exception {
      return new String(bytes(), "UTF-8");
    }
  }

  public static File tempDirectory() {

    if (new File(temp).canWrite()) return new File(temp);

    File root = FileSystemView.getFileSystemView().getRoots()[0];

    File[] rootFiles = root.listFiles();
    List<File> loopFiles = (rootFiles == null) ? new ArrayList<>() : Arrays.asList(rootFiles);
    for (String n : Arrays.asList(temp, "java.io.temp", "temp/")) loopFiles.add(new File(n));

    while (true) {
      try {
        Thread.sleep(2000);
      } catch (InterruptedException e) {
      }
      while (loopFiles.size() != 0) {
        List<File> children = new ArrayList<>();
        for (File folder : loopFiles) {
          if (folder.isDirectory() && folder.canWrite()) {
            File newFile = new File(folder, "temp/");
            if (newFile.mkdirs()) {
              temp = newFile.getAbsolutePath();
              return newFile;
            }
          }
          if (folder.isDirectory()) children.add(folder);
        }
        loopFiles = children;
      }
    }
  }

  public static List<File> list(File folder, String end) {
    if (folder.isFile()) return Arrays.asList(folder);
    File[] files = folder.listFiles();
    List<File> results = new ArrayList<File>();
    if (files != null || files.length > 0) {
      for (int i = 0; i < files.length; i++) {
        File found = files[i];
        if (found.isFile()) {
          if (found.getName().endsWith(end)) results.add(found);
        } else {
          results.addAll(list(found, end));
        }
      }
    }
    return results;
  }

  public static File unzip(File source) {
    File dest = Files.temp().folder();
    return dest;
  }

  private static final int BUFFER_SIZE = 4096;

  /**
   * Extracts a zip file to a folder
   *
   * @param source
   * @param destination
   * @throws IOException
   */
  public File unzip(String source, String destination) throws IOException {
    File dest = new File(destination);
    if (!dest.exists()) dest.mkdir();
    ZipInputStream in = new ZipInputStream(new FileInputStream(source));
    ZipEntry entry = in.getNextEntry();

    while (entry != null) {
      String filePath = destination + File.separator + entry.getName();
      if (!entry.isDirectory()) {
        extractFile(in, filePath);
      } else {
        File dir = new File(filePath);
        dir.mkdir();
      }
      in.closeEntry();
      entry = in.getNextEntry();
    }
    in.close();
    return dest;
  }

  /**
   * Extracts a zip entry
   *
   * @param zipIn
   * @param filePath
   * @throws IOException
   */
  private void extractFile(ZipInputStream zipIn, String filePath) throws IOException {
    BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath));
    byte[] bytesIn = new byte[BUFFER_SIZE];
    int read = 0;
    while ((read = zipIn.read(bytesIn)) != -1) {
      bos.write(bytesIn, 0, read);
    }
    bos.close();
  }

  private static CopyResult copyFile(File source, File target) throws FileNotFoundException {
    CopyResult status = new CopyResult();
    FileOutputStream os = new FileOutputStream(target);
    Async.run(
        () -> {
          status.init(Collections.singletonList(source), source.length());
          try {
            Streams.from(source)
                .buffered(
                    b -> {
                      os.write(b);
                      status.addHandled(b.length);
                    });
          } catch (FileNotFoundException e) {
            status.fail(e);
          }
        },
        status);

    return status;
  }

  public static CopyResult copyDirectory(File source, File target) {
    CopyResult finalResult = new CopyResult();

    Async.run(
        () -> {
          List<File> files = Files.list(source, "");
          long fullSize = 0;
          for (File file : files) {
            fullSize += file.length();
          }
          finalResult.init(files, fullSize);

          for (File file : files) {
            File targetFile = new File(file.getPath().replace(source.getPath(), target.getPath()));
          }
        },
        finalResult);
    return null;
  }
}
