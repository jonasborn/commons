package de.obdev.commons.io;

import de.obdev.commons.async.AsyncResult;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CopyResult extends AsyncResult<File> {

  private File current;

  private long handledBytes;
  private long completeBytes;

  private List<File> pendingFiles = new ArrayList<>();

  public void init(List<File> files, long completeBytes) {
    this.pendingFiles = files;
    this.completeBytes = completeBytes;
  }

  public void setCurrent(File current) {
    this.current = current;
    this.pendingFiles.remove(current);
  }

  public void addHandled(long handled) {
    this.handledBytes = +handled;
  }

  public double getProgress() {
    double a = 100 / completeBytes;
    return a * handledBytes;
  }

  public long handledBytes() {
    return handledBytes;
  }

  public File getCurrent() {
    return current;
  }

  public long getHandledBytes() {
    return handledBytes;
  }

  public long getCompleteBytes() {
    return completeBytes;
  }

  public List<File> getPendingFiles() {
    return pendingFiles;
  }
}
