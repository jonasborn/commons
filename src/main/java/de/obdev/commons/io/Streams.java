package de.obdev.commons.io;

import de.obdev.commons.Charsets;
import de.obdev.commons.convert.Convertable;

import java.io.*;
import java.net.Socket;
import java.nio.charset.Charset;

public class Streams {

  public static ReadableResource from(InputStream is) {
    return new ReadableResource(is);
  }

  public static ReadableResource from(File file) throws FileNotFoundException {
    return from(new FileInputStream(file));
  }

  public static ReadableResource from(Socket socket) throws IOException {
    return from(socket.getInputStream());
  }

  public static ReadableResource from(byte[] array) {
    return from(new ByteArrayInputStream(array));
  }

  public static ReadableResource from(String s, Charset charset) {
    return from(s.getBytes(charset));
  }

  public static ReadableResource from(String s) {
    return from(s, Charsets.UTF8);
  }

  public static ReadableResource from(Convertable convertable) {
    return from(convertable.asBytes());
  }

  // WRITE SECTION

  public static WritableResource to(OutputStream os) {
    return new WritableResource(os);
  }

  public static WritableResource to(File f) throws FileNotFoundException {
    return to(new FileOutputStream(f));
  }

  public static WritableResource to(Socket socket) throws IOException {
    return to(socket.getOutputStream());
  }
}
