package de.obdev.commons.io;

import de.obdev.commons.Charsets;
import de.obdev.commons.async.Async;
import de.obdev.commons.async.AsyncResult;
import de.obdev.commons.util.UnsafeConsumer;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

public class ReadableResource {

  InputStream is;
  static int defaultBufferSize = 2048;

  Function<byte[], byte[]> modifier;
  Function<Throwable, byte[]> handler;

  public static int getDefaultBufferSize() {
    return defaultBufferSize;
  }

  public static void setDefaultBufferSize(int defaultBufferSize) {
    ReadableResource.defaultBufferSize = defaultBufferSize;
  }

  public ReadableResource(InputStream is) {
    this.is = is;
  }

  /**
   * Can be used to instant modify single parts decode from a stream If a modifier is set, it always
   * will be used
   *
   * @param function
   * @return The current instance
   */
  public ReadableResource modifier(Function<byte[], byte[]> function) {
    this.modifier = function;
    return this;
  }

  /**
   * One way to easily catch thrown exceptions and give an alternative return value
   *
   * @param function
   * @return The current instance
   */
  public ReadableResource handler(Function<Throwable, byte[]> function) {
    this.handler = function;
    return this;
  }

  private byte[] handle(Throwable t) {
    if (handler != null) return modify(this.handler.apply(t));
    return null;
  }

  private byte[] modify(byte[] in) {
    if (modifier != null) return modifier.apply(in);
    return in;
  }

  /**
   * Reads a stream buffered and slitted in to smaller packets
   *
   * @param consumer
   * @throws IOException
   */
  public AsyncResult<Object> buffered(UnsafeConsumer<byte[]> consumer, int bufferSize) {
    return Async.run(
        result -> {
          int i = 0;
          byte[] buffer = new byte[bufferSize];
          try {
            while ((i = is.read(buffer)) != -1) {
              byte[] target = new byte[i];
              System.arraycopy(buffer, 0, target, 0, i);
              consumer.handle(modify(target));
            }
            result.succeed();
          } catch (IOException e) {
            try {
              consumer.handle(handle(e));
            } catch (Exception e1) {
              result.fail(e);
            }
            result.fail(e);
          } catch (Exception e) {
            result.fail(e);
          }
          return null;
        });
  }

  public AsyncResult<Object> buffered(UnsafeConsumer<byte[]> consumer) {
    return this.buffered(consumer, defaultBufferSize);
  }

  /**
   * Reads length fist described packets from current stream
   *
   * @param function A function called when a new packet has been accepted. The result of the
   *     function indicates whether the function should continue to be decode
   * @throws IOException
   */
  public AsyncResult<Object> packed(
      Function<byte[], Boolean> function, int bufferSize, ByteOrder order) {
    AsyncResult<Object> r =
        Async.run(
            result -> {
              boolean run = true;
              while (!result.finished()) {
                Integer bytesRead;
                byte[] request = new byte[bufferSize];
                int read = 0;
                int size = 0;
                ByteArrayOutputStream bout = new ByteArrayOutputStream();
                try {
                  while (run && (bytesRead = is.read(request)) != -1) {
                    if (size == 0) {
                      ByteBuffer buffer = ByteBuffer.wrap(request);
                      buffer.order(order);
                      size = buffer.getInt();
                    }
                    bout.write(request, 0, bytesRead);
                    read = read + bytesRead;
                    if (read >= size) {
                      run = function.apply(bout.toByteArray());
                      bout.reset();

                      size = 0;
                      read = 0;
                    }
                  }
                  function.apply(null);

                  break;
                } catch (Exception e) {
                  function.apply(handle(e));
                  result.fail(e);
                  break;
                }
              }
              result.succeed();
              // Return something to match the function requirements
              return null;
            });
    return r;
  }

  public AsyncResult packed(Function<byte[], Boolean> function) {
    return this.packed(function, defaultBufferSize, ByteOrder.LITTLE_ENDIAN);
  }

  /**
   * Reads the stream in a String
   *
   * @param charset A charset to be used to encode the bytes
   * @return The InputSteam as String
   * @throws IOException
   * @see #bytes()
   */
  public String string(Charset charset) throws Exception {
    return new String(bytes().await().get(), charset);
  }

  public String string() throws Exception {
    return new String(bytes().await().get(), Charsets.DEFAULT);
  }

  public InputStream stream() {
    return is;
  }

  /**
   * Reads a stream buffered and stores its parts in a single convert bytes
   *
   * @return A bytes of bytes, probably modified by the given modifier
   * @see #buffered(UnsafeConsumer)
   */
  public AsyncResult<byte[]> bytes() {
    ByteArrayOutputStream bout = new ByteArrayOutputStream();
    AsyncResult<byte[]> result = new AsyncResult<>();
    try {
      this.buffered(
              b -> {
                try {
                  bout.write(b);
                } catch (IOException e) {
                  result.fail(e);
                }
              })
          .await();
    } catch (Exception e) {
      result.fail(e);
    }
    result.succeed(bout.toByteArray());
    return result;
  }

  /**
   * Reads a stream buffered and stores its parts in to a list
   *
   * @return A List of convert arrays
   */
  public List<byte[]> list() {
    List<byte[]> list = new ArrayList<>();
    try {
      this.buffered(list::add).await();
    } catch (Throwable throwable) {
      throwable.printStackTrace();
    }
    return list;
  }

  public AsyncResult<File> file(File target) throws IOException {
    FileOutputStream fout = new FileOutputStream(target);
    AsyncResult<File> result = new AsyncResult<>();
    try {
      buffered(
              b -> {
                try {
                  fout.write(b);
                } catch (IOException e) {
                  result.fail(e);
                }
              })
          .await();
    } catch (Exception e) {
      result.fail(e);
    }
    result.succeed(target);
    return result;
    //TODO Beautify this!
  }

  public AsyncResult<File> file() throws IOException {
    return file(Files.temp().file());
  }

  @Override
  public String toString() {
    try {
      return string();
    } catch (Exception e) {
      e.printStackTrace();
    }
    return null;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ReadableResource that = (ReadableResource) o;
    return Objects.equals(is, that.is)
        && Objects.equals(modifier, that.modifier)
        && Objects.equals(handler, that.handler);
  }

  @Override
  public int hashCode() {

    return Objects.hash(is, modifier, handler);
  }
}
