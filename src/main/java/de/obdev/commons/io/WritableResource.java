package de.obdev.commons.io;

import de.obdev.commons.Charsets;
import de.obdev.commons.Format;
import de.obdev.commons.async.Async;
import de.obdev.commons.async.AsyncResult;
import de.obdev.commons.convert.Convertable;
import de.obdev.commons.util.Nullable;

import javax.imageio.ImageIO;
import java.awt.image.RenderedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.util.function.Function;

public class WritableResource {

  OutputStream os;

  Function<byte[], byte[]> modifier;

  public WritableResource(OutputStream os) {
    this.os = os;
  }

  @Nullable
  public AsyncResult<Object> write(ReadableResource resource) {
    return resource.buffered(os::write);
  }

  public AsyncResult<Object> write(Convertable convertable) {
    if (modifier == null) {
      return Streams.from(convertable.asBytes()).buffered(os::write);
    } else {
      return Streams.from(convertable.asBytes()).modifier(this.modifier).buffered(os::write);
    }
  }

  @Nullable
  public AsyncResult<Object> write(InputStream is) throws IOException {
    if (modifier == null) {
      return Streams.from(is).buffered(os::write);
    } else {
      return Streams.from(is).modifier(this.modifier).buffered(os::write);
    }
  }

  @Nullable
  public AsyncResult<Object> write(File file) throws IOException {
    return write(new FileInputStream(file));
  }

  @Nullable
  public AsyncResult<Object> write(RenderedImage image, Format format) {
    return Async.run(
        result -> {
          ImageIO.write(image, format.getExtension(), os);
          return null;
        });
  }

  public AsyncResult<Object> write(byte[] array) throws IOException {
    ByteArrayInputStream is = new ByteArrayInputStream(array);
    return write(is);
  }

  public AsyncResult<Object> write(String s, Charset charset) throws IOException {
    return write(s.getBytes(charset));
  }

  public AsyncResult<Object> write(String s) throws IOException {
    return write(s, Charsets.DEFAULT);
  }
}
