package de.obdev.commons.io;

import java.io.InputStream;
import java.io.OutputStream;

public class IOPipe implements Runnable {

    InputStream is;
    OutputStream os;

    boolean run = true;

    public IOPipe(InputStream is, OutputStream os) {
        this.is = is;
        this.os = os;
    }

    @Override
    public void run() {
        while (run) {
            try {
                int read = 0;
                byte[] buff = new byte[1024];
                while (run && (read = is.read(buff)) != -1) {
                    os.write(buff, 0, read);
                }
            } catch (Exception e) {

            }
        }
    }

    public void stop() {
        this.run = false;
    }
}
