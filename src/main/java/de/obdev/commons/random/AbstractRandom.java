package de.obdev.commons.random;

import de.obdev.commons.Charsets;
import de.obdev.commons.convert.Convertable;

import java.nio.charset.Charset;

public abstract class AbstractRandom {

  public abstract byte[] getBytes(int len);

  public Convertable getConvertable(int length) {
    return Convertable.of(getBytes(length));
  }

  public short getShort() {
    return getConvertable(2).asShort();
  }

  public short[] getShorts(int amount) {
    short[] res = new short[amount];
    for (int i = 0; i < amount; i++) {
      res[i] = getShort();
    }
    return res;
  }

  public int getInt() {
    return getConvertable(4).asInt();
  }

  public float getFloat() {
    return getConvertable(4).asFloat();
  }

  public double getDouble() {
    return getConvertable(8).asDouble();
  }

  public long getLong() {
    return getConvertable(8).asLong();
  }

  public char getChar() {
    return getConvertable(2).asChar();
  }

  public String getString(Integer length, Charset charset) {
    return getConvertable(length * 2).asString(charset);
  }

  public String getString(Integer length) {
    return getString(length, Charsets.DEFAULT);
  }
}
