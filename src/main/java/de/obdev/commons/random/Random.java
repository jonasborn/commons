package de.obdev.commons.random;

public class Random extends AbstractRandom {

  static java.util.Random random = new java.util.Random();

  @Override
  public byte[] getBytes(int len) {
    byte[] res = new byte[len];
    random.nextBytes(res);
    return res;
  }
}
