package de.obdev.commons.random;

public class SecureRandom extends AbstractRandom {

  static java.security.SecureRandom random = new java.security.SecureRandom();

  @Override
  public byte[] getBytes(int len) {
    byte[] res = new byte[len];
    random.nextBytes(res);
    return res;
  }
}
