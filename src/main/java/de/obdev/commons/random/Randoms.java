package de.obdev.commons.random;

public class Randoms {

  public static SecureRandom secure() {
    return new SecureRandom();
  }

  public static Random unsecure() {
    return new Random();
  }
}
