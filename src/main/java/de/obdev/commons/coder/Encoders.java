package de.obdev.commons.coder;

import de.obdev.commons.coder.base.Base64Encoder;
import de.obdev.commons.coder.json.JsonEncoderBuilder;
import de.obdev.commons.coder.model.ModelEncoder;
import de.obdev.commons.coder.url.UrlEncoder;
import de.obdev.commons.coder.yaml.YamlEncoder;

public class Encoders {

  public static JsonEncoderBuilder json() {
    return new JsonEncoderBuilder();
  }

  public static Base64Encoder base64() {
    return new Base64Encoder();
  }

  public static YamlEncoder yaml() {
    return new YamlEncoder();
  }

  public static UrlEncoder url() {
    return new UrlEncoder();
  }

  public static ModelEncoder model() {
    return new ModelEncoder();
  }
}
