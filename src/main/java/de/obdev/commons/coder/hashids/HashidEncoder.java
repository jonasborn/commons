package de.obdev.commons.coder.hashids;

public class HashidEncoder {

  public String encode(long... numbers) {
    return HashidsProvider.getDefaultProvider().encode(numbers);
  }

  public String encode(int... numbers) {
    long[] longs = new long[numbers.length];
    for (int i = 0; i < numbers.length; i++) {
      longs[i] = (long) numbers[i];
    }
    return HashidsProvider.getDefaultProvider().encode(longs);
  }
}
