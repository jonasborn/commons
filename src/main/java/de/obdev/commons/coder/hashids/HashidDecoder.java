package de.obdev.commons.coder.hashids;

public class HashidDecoder {

  public long[] decode(String hashid) {
    return HashidsProvider.getDefaultProvider().decode(hashid);
  }
}
