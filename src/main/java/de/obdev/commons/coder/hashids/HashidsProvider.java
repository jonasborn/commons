package de.obdev.commons.coder.hashids;

import org.hashids.Hashids;

public class HashidsProvider {

  static Hashids defaultProvider = new Hashids();

  public static Hashids getDefaultProvider() {
    return defaultProvider;
  }

  public static void setDefaultProvider(Hashids defaultProvider) {
    HashidsProvider.defaultProvider = defaultProvider;
  }
}
