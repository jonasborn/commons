package de.obdev.commons.coder.model;

import de.obdev.commons.convert.Convertable;
import de.obdev.commons.model.Model;

import java.util.Map;

public class ModelEncoder {

  public Model encode(Object o) {
    Convertable convertable = ModelProvider.getDefaultEncoder().encode(o);
    Map<String, Object> map = ModelProvider.getDefaultDecoder().decode(convertable);
    return new Model(map);
  }
}
