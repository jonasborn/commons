package de.obdev.commons.coder.model;

import de.obdev.commons.convert.Convertable;
import de.obdev.commons.model.Model;
import de.obdev.commons.util.TypeDef;

public class ModelDecoder {

  public <T> T decode(Model model, TypeDef<T> typeDef) {
    Convertable convertable = ModelProvider.getDefaultEncoder().encode(model);
    return ModelProvider.getDefaultDecoder().decode(convertable, typeDef);
  }

  public <T> T decode(Model model) {
    TypeDef<T> typeDef = new TypeDef<T>() {};
    return decode(model, typeDef);
  }
}
