package de.obdev.commons.coder.model;

import de.obdev.commons.coder.Decoders;
import de.obdev.commons.coder.Encoders;
import de.obdev.commons.coder.json.JsonDecoder;
import de.obdev.commons.coder.json.JsonEncoder;

public class ModelProvider {

  static JsonEncoder DEFAULT_ENCODER = Encoders.json().create();
  static JsonDecoder DEFAULT_DECODER = Decoders.json().create();

  public static JsonEncoder getDefaultEncoder() {
    return DEFAULT_ENCODER;
  }

  public static void setDefaultEncoder(JsonEncoder defaultEncoder) {
    DEFAULT_ENCODER = defaultEncoder;
  }

  public static JsonDecoder getDefaultDecoder() {
    return DEFAULT_DECODER;
  }

  public static void setDefaultDecoder(JsonDecoder defaultDecoder) {
    DEFAULT_DECODER = defaultDecoder;
  }
}
