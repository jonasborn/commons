package de.obdev.commons.coder.json;

import com.owlike.genson.*;
import com.owlike.genson.convert.ChainedFactory;
import com.owlike.genson.convert.ContextualFactory;
import com.owlike.genson.ext.GensonBundle;
import com.owlike.genson.reflect.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;

public class JsonDecoderBuilder {

  GensonBuilder builder = JsonProvider.getDefaultProvider();

  public JsonDecoderBuilder() {}

  public JsonDecoderBuilder addAlias(String alias, Class<?> forClass) {
    builder.addAlias(alias, forClass);
    return this;
  }

  public JsonDecoderBuilder withConverters(Converter<?>... converter) {
    builder.withConverters(converter);
    return this;
  }

  public <T> JsonDecoderBuilder withConverter(Converter<T> converter, Class<? extends T> type) {
    builder.withConverter(converter, type);
    return this;
  }

  public <T> JsonDecoderBuilder withConverter(
      Converter<T> converter, GenericType<? extends T> type) {
    builder.withConverter(converter, type);
    return this;
  }

  public JsonDecoderBuilder withSerializers(Serializer<?>... serializer) {
    builder.withSerializers(serializer);
    return this;
  }

  public <T> JsonDecoderBuilder withSerializer(Serializer<T> serializer, Class<? extends T> type) {
    builder.withSerializer(serializer, type);
    return this;
  }

  public <T> JsonDecoderBuilder withSerializer(
      Serializer<T> serializer, GenericType<? extends T> type) {
    builder.withSerializer(serializer, type);
    return this;
  }

  public JsonDecoderBuilder withDeserializers(Deserializer<?>... deserializer) {
    builder.withDeserializers(deserializer);
    return this;
  }

  public <T> JsonDecoderBuilder withDeserializer(
      Deserializer<T> deserializer, Class<? extends T> type) {
    builder.withDeserializer(deserializer, type);
    return this;
  }

  public <T> JsonDecoderBuilder withDeserializer(
      Deserializer<T> deserializer, GenericType<? extends T> type) {
    builder.withDeserializer(deserializer, type);
    return this;
  }

  public JsonDecoderBuilder withConverterFactory(Factory<? extends Converter<?>> factory) {
    builder.withConverterFactory(factory);
    return this;
  }

  public JsonDecoderBuilder withSerializerFactory(Factory<? extends Serializer<?>> factory) {
    builder.withSerializerFactory(factory);
    return this;
  }

  public JsonDecoderBuilder withDeserializerFactory(Factory<? extends Deserializer<?>> factory) {
    builder.withDeserializerFactory(factory);
    return this;
  }

  public JsonDecoderBuilder withContextualFactory(ContextualFactory<?>... factories) {
    builder.withContextualFactory(factories);
    return this;
  }

  public JsonDecoderBuilder withConverterFactory(ChainedFactory chainedFactory) {
    builder.withConverterFactory(chainedFactory);
    return this;
  }

  public JsonDecoderBuilder withBeanPropertyFactory(BeanPropertyFactory... factories) {
    builder.withBeanPropertyFactory(factories);
    return this;
  }

  public JsonDecoderBuilder withBundle(GensonBundle... bundles) {
    builder.withBundle(bundles);
    return this;
  }

  public JsonDecoderBuilder withClassLoader(ClassLoader loader) {
    builder.withClassLoader(loader);
    return this;
  }

  public JsonDecoderBuilder set(BeanMutatorAccessorResolver resolver) {
    builder.set(resolver);
    return this;
  }

  public JsonDecoderBuilder set(PropertyNameResolver resolver) {
    builder.set(resolver);
    return this;
  }

  public JsonDecoderBuilder with(BeanMutatorAccessorResolver... resolvers) {
    builder.with(resolvers);
    return this;
  }

  public Map<Type, Serializer<?>> getSerializersMap() {
    return builder.getSerializersMap();
  }

  public Map<Type, Deserializer<?>> getDeserializersMap() {
    return builder.getDeserializersMap();
  }

  public ClassLoader getClassLoader() {
    return builder.getClassLoader();
  }

  public JsonDecoderBuilder with(PropertyNameResolver... resolvers) {
    builder.with(resolvers);
    return this;
  }

  public JsonDecoderBuilder rename(String field, String toName) {
    builder.rename(field, toName);
    return this;
  }

  public JsonDecoderBuilder rename(Class<?> fieldOfType, String toName) {
    builder.rename(fieldOfType, toName);
    return this;
  }

  public JsonDecoderBuilder rename(String field, Class<?> fromClass, String toName) {
    builder.rename(field, fromClass, toName);
    return this;
  }

  public JsonDecoderBuilder rename(String field, String toName, Class<?> fieldOfType) {
    builder.rename(field, toName, fieldOfType);
    return this;
  }

  public JsonDecoderBuilder rename(
      String field, Class<?> fromClass, String toName, Class<?> ofType) {
    builder.rename(field, fromClass, toName, ofType);
    return this;
  }

  public JsonDecoderBuilder exclude(String field) {
    builder.exclude(field);
    return this;
  }

  public JsonDecoderBuilder exclude(Class<?> fieldOfType) {
    builder.exclude(fieldOfType);
    return this;
  }

  public JsonDecoderBuilder exclude(String field, Class<?> fromClass) {
    builder.exclude(field, fromClass);
    return this;
  }

  public JsonDecoderBuilder exclude(String field, Class<?> fromClass, Class<?> ofType) {
    builder.exclude(field, fromClass, ofType);
    return this;
  }

  public JsonDecoderBuilder include(String field) {
    builder.include(field);
    return this;
  }

  public JsonDecoderBuilder include(Class<?> fieldOfType) {
    builder.include(fieldOfType);
    return this;
  }

  public JsonDecoderBuilder include(String field, Class<?> fromClass) {
    builder.include(field, fromClass);
    return this;
  }

  public JsonDecoderBuilder include(String field, Class<?> fromClass, Class<?> ofType) {
    builder.include(field, fromClass, ofType);
    return this;
  }

  public boolean isSkipNull() {
    return builder.isSkipNull();
  }

  public JsonDecoderBuilder setSkipNull(boolean skipNull) {
    builder.setSkipNull(skipNull);
    return this;
  }

  public boolean isHtmlSafe() {
    return builder.isHtmlSafe();
  }

  public JsonDecoderBuilder setHtmlSafe(boolean htmlSafe) {
    builder.setHtmlSafe(htmlSafe);
    return this;
  }

  public JsonDecoderBuilder useClassMetadata(boolean enabled) {
    builder.useClassMetadata(enabled);
    return this;
  }

  public JsonDecoderBuilder useDateFormat(DateFormat dateFormat) {
    builder.useDateFormat(dateFormat);
    return this;
  }

  public boolean isThrowExceptionOnNoDebugInfo() {
    return builder.isThrowExceptionOnNoDebugInfo();
  }

  public JsonDecoderBuilder setThrowExceptionIfNoDebugInfo(boolean throwExcOnNoDebugInfo) {
    builder.setThrowExceptionIfNoDebugInfo(throwExcOnNoDebugInfo);
    return this;
  }

  public JsonDecoderBuilder useMethods(boolean enabled) {
    builder.useMethods(enabled);
    return this;
  }

  public JsonDecoderBuilder useMethods(boolean enabled, VisibilityFilter visibility) {
    builder.useMethods(enabled, visibility);
    return this;
  }

  public JsonDecoderBuilder useFields(boolean enabled) {
    builder.useFields(enabled);
    return this;
  }

  public JsonDecoderBuilder useFields(boolean enabled, VisibilityFilter visibility) {
    builder.useFields(enabled, visibility);
    return this;
  }

  public JsonDecoderBuilder useBeanViews(boolean enabled) {
    builder.useBeanViews(enabled);
    return this;
  }

  public JsonDecoderBuilder useRuntimeType(boolean enabled) {
    builder.useRuntimeType(enabled);
    return this;
  }

  public JsonDecoderBuilder useConstructorWithArguments(boolean enabled) {
    builder.useConstructorWithArguments(enabled);
    return this;
  }

  public JsonDecoderBuilder setFieldFilter(VisibilityFilter propertyFilter) {
    builder.setFieldFilter(propertyFilter);
    return this;
  }

  public JsonDecoderBuilder setMethodFilter(VisibilityFilter methodFilter) {
    builder.setMethodFilter(methodFilter);
    return this;
  }

  public JsonDecoderBuilder setConstructorFilter(VisibilityFilter constructorFilter) {
    builder.setConstructorFilter(constructorFilter);
    return this;
  }

  public JsonDecoderBuilder useStrictDoubleParse(boolean strictDoubleParse) {
    builder.useStrictDoubleParse(strictDoubleParse);
    return this;
  }

  public JsonDecoderBuilder useIndentation(boolean indent) {
    builder.useIndentation(indent);
    return this;
  }

  public JsonDecoderBuilder useDateAsTimestamp(boolean enabled) {
    builder.useDateAsTimestamp(enabled);
    return this;
  }

  public JsonDecoderBuilder useMetadata(boolean metadata) {
    builder.useMetadata(metadata);
    return this;
  }

  public JsonDecoderBuilder useByteAsInt(boolean enable) {
    builder.useByteAsInt(enable);
    return this;
  }

  public JsonDecoderBuilder failOnMissingProperty(boolean enable) {
    builder.failOnMissingProperty(enable);
    return this;
  }

  public JsonDecoderBuilder useClassMetadataWithStaticType(boolean enable) {
    builder.useClassMetadataWithStaticType(enable);
    return this;
  }

  public JsonDecoderBuilder acceptSingleValueAsList(boolean enable) {
    builder.acceptSingleValueAsList(enable);
    return this;
  }

  public JsonDecoderBuilder useDefaultValue(Object value, Class<?> targetType) {
    builder.useDefaultValue(value, targetType);
    return this;
  }

  public JsonDecoderBuilder wrapRootValues(String inputKey, String outputKey) {
    builder.wrapRootValues(inputKey, outputKey);
    return this;
  }

  public JsonDecoderBuilder failOnNullPrimitive(boolean enabled) {
    builder.failOnNullPrimitive(enabled);
    return this;
  }

  public JsonDecoderBuilder useRuntimePropertyFilter(RuntimePropertyFilter filter) {
    builder.useRuntimePropertyFilter(filter);
    return this;
  }

  public JsonDecoder create() {
    return new JsonDecoder(builder.create());
  }

  public List<Factory<?>> getFactories() {
    return builder.getFactories();
  }

  public boolean isDateAsTimestamp() {
    return builder.isDateAsTimestamp();
  }
}
