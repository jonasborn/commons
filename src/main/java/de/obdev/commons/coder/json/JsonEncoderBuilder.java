package de.obdev.commons.coder.json;

import com.owlike.genson.*;
import com.owlike.genson.convert.ChainedFactory;
import com.owlike.genson.convert.ContextualFactory;
import com.owlike.genson.ext.GensonBundle;
import com.owlike.genson.reflect.*;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.util.List;
import java.util.Map;

public class JsonEncoderBuilder {

  GensonBuilder builder = JsonProvider.getDefaultProvider();

  public JsonEncoderBuilder() {}

  public JsonEncoderBuilder addAlias(String alias, Class<?> forClass) {
    builder.addAlias(alias, forClass);
    return this;
  }

  public JsonEncoderBuilder withConverters(Converter<?>... converter) {
    builder.withConverters(converter);
    return this;
  }

  public <T> JsonEncoderBuilder withConverter(Converter<T> converter, Class<? extends T> type) {
    builder.withConverter(converter, type);
    return this;
  }

  public <T> JsonEncoderBuilder withConverter(
      Converter<T> converter, GenericType<? extends T> type) {
    builder.withConverter(converter, type);
    return this;
  }

  public JsonEncoderBuilder withSerializers(Serializer<?>... serializer) {
    builder.withSerializers(serializer);
    return this;
  }

  public <T> JsonEncoderBuilder withSerializer(Serializer<T> serializer, Class<? extends T> type) {
    builder.withSerializer(serializer, type);
    return this;
  }

  public <T> JsonEncoderBuilder withSerializer(
      Serializer<T> serializer, GenericType<? extends T> type) {
    builder.withSerializer(serializer, type);
    return this;
  }

  public JsonEncoderBuilder withDeserializers(Deserializer<?>... deserializer) {
    builder.withDeserializers(deserializer);
    return this;
  }

  public <T> JsonEncoderBuilder withDeserializer(
      Deserializer<T> deserializer, Class<? extends T> type) {
    builder.withDeserializer(deserializer, type);
    return this;
  }

  public <T> JsonEncoderBuilder withDeserializer(
      Deserializer<T> deserializer, GenericType<? extends T> type) {
    builder.withDeserializer(deserializer, type);
    return this;
  }

  public JsonEncoderBuilder withConverterFactory(Factory<? extends Converter<?>> factory) {
    builder.withConverterFactory(factory);
    return this;
  }

  public JsonEncoderBuilder withSerializerFactory(Factory<? extends Serializer<?>> factory) {
    builder.withSerializerFactory(factory);
    return this;
  }

  public JsonEncoderBuilder withDeserializerFactory(Factory<? extends Deserializer<?>> factory) {
    builder.withDeserializerFactory(factory);
    return this;
  }

  public JsonEncoderBuilder withContextualFactory(ContextualFactory<?>... factories) {
    builder.withContextualFactory(factories);
    return this;
  }

  public JsonEncoderBuilder withConverterFactory(ChainedFactory chainedFactory) {
    builder.withConverterFactory(chainedFactory);
    return this;
  }

  public JsonEncoderBuilder withBeanPropertyFactory(BeanPropertyFactory... factories) {
    builder.withBeanPropertyFactory(factories);
    return this;
  }

  public JsonEncoderBuilder withBundle(GensonBundle... bundles) {
    builder.withBundle(bundles);
    return this;
  }

  public JsonEncoderBuilder withClassLoader(ClassLoader loader) {
    builder.withClassLoader(loader);
    return this;
  }

  public JsonEncoderBuilder set(BeanMutatorAccessorResolver resolver) {
    builder.set(resolver);
    return this;
  }

  public JsonEncoderBuilder set(PropertyNameResolver resolver) {
    builder.set(resolver);
    return this;
  }

  public JsonEncoderBuilder with(BeanMutatorAccessorResolver... resolvers) {
    builder.with(resolvers);
    return this;
  }

  public Map<Type, Serializer<?>> getSerializersMap() {
    return builder.getSerializersMap();
  }

  public Map<Type, Deserializer<?>> getDeserializersMap() {
    return builder.getDeserializersMap();
  }

  public ClassLoader getClassLoader() {
    return builder.getClassLoader();
  }

  public JsonEncoderBuilder with(PropertyNameResolver... resolvers) {
    builder.with(resolvers);
    return this;
  }

  public JsonEncoderBuilder rename(String field, String toName) {
    builder.rename(field, toName);
    return this;
  }

  public JsonEncoderBuilder rename(Class<?> fieldOfType, String toName) {
    builder.rename(fieldOfType, toName);
    return this;
  }

  public JsonEncoderBuilder rename(String field, Class<?> fromClass, String toName) {
    builder.rename(field, fromClass, toName);
    return this;
  }

  public JsonEncoderBuilder rename(String field, String toName, Class<?> fieldOfType) {
    builder.rename(field, toName, fieldOfType);
    return this;
  }

  public JsonEncoderBuilder rename(
      String field, Class<?> fromClass, String toName, Class<?> ofType) {
    builder.rename(field, fromClass, toName, ofType);
    return this;
  }

  public JsonEncoderBuilder exclude(String field) {
    builder.exclude(field);
    return this;
  }

  public JsonEncoderBuilder exclude(Class<?> fieldOfType) {
    builder.exclude(fieldOfType);
    return this;
  }

  public JsonEncoderBuilder exclude(String field, Class<?> fromClass) {
    builder.exclude(field, fromClass);
    return this;
  }

  public JsonEncoderBuilder exclude(String field, Class<?> fromClass, Class<?> ofType) {
    builder.exclude(field, fromClass, ofType);
    return this;
  }

  public JsonEncoderBuilder include(String field) {
    builder.include(field);
    return this;
  }

  public JsonEncoderBuilder include(Class<?> fieldOfType) {
    builder.include(fieldOfType);
    return this;
  }

  public JsonEncoderBuilder include(String field, Class<?> fromClass) {
    builder.include(field, fromClass);
    return this;
  }

  public JsonEncoderBuilder include(String field, Class<?> fromClass, Class<?> ofType) {
    builder.include(field, fromClass, ofType);
    return this;
  }

  public boolean isSkipNull() {
    return builder.isSkipNull();
  }

  public JsonEncoderBuilder setSkipNull(boolean skipNull) {
    builder.setSkipNull(skipNull);
    return this;
  }

  public boolean isHtmlSafe() {
    return builder.isHtmlSafe();
  }

  public JsonEncoderBuilder setHtmlSafe(boolean htmlSafe) {
    builder.setHtmlSafe(htmlSafe);
    return this;
  }

  public JsonEncoderBuilder useClassMetadata(boolean enabled) {
    builder.useClassMetadata(enabled);
    return this;
  }

  public JsonEncoderBuilder useDateFormat(DateFormat dateFormat) {
    builder.useDateFormat(dateFormat);
    return this;
  }

  public boolean isThrowExceptionOnNoDebugInfo() {
    return builder.isThrowExceptionOnNoDebugInfo();
  }

  public JsonEncoderBuilder setThrowExceptionIfNoDebugInfo(boolean throwExcOnNoDebugInfo) {
    builder.setThrowExceptionIfNoDebugInfo(throwExcOnNoDebugInfo);
    return this;
  }

  public JsonEncoderBuilder useMethods(boolean enabled) {
    builder.useMethods(enabled);
    return this;
  }

  public JsonEncoderBuilder useMethods(boolean enabled, VisibilityFilter visibility) {
    builder.useMethods(enabled, visibility);
    return this;
  }

  public JsonEncoderBuilder useFields(boolean enabled) {
    builder.useFields(enabled);
    return this;
  }

  public JsonEncoderBuilder useFields(boolean enabled, VisibilityFilter visibility) {
    builder.useFields(enabled, visibility);
    return this;
  }

  public JsonEncoderBuilder useBeanViews(boolean enabled) {
    builder.useBeanViews(enabled);
    return this;
  }

  public JsonEncoderBuilder useRuntimeType(boolean enabled) {
    builder.useRuntimeType(enabled);
    return this;
  }

  public JsonEncoderBuilder useConstructorWithArguments(boolean enabled) {
    builder.useConstructorWithArguments(enabled);
    return this;
  }

  public JsonEncoderBuilder setFieldFilter(VisibilityFilter propertyFilter) {
    builder.setFieldFilter(propertyFilter);
    return this;
  }

  public JsonEncoderBuilder setMethodFilter(VisibilityFilter methodFilter) {
    builder.setMethodFilter(methodFilter);
    return this;
  }

  public JsonEncoderBuilder setConstructorFilter(VisibilityFilter constructorFilter) {
    builder.setConstructorFilter(constructorFilter);
    return this;
  }

  public JsonEncoderBuilder useStrictDoubleParse(boolean strictDoubleParse) {
    builder.useStrictDoubleParse(strictDoubleParse);
    return this;
  }

  public JsonEncoderBuilder useIndentation(boolean indent) {
    builder.useIndentation(indent);
    return this;
  }

  public JsonEncoderBuilder useDateAsTimestamp(boolean enabled) {
    builder.useDateAsTimestamp(enabled);
    return this;
  }

  public JsonEncoderBuilder useMetadata(boolean metadata) {
    builder.useMetadata(metadata);
    return this;
  }

  public JsonEncoderBuilder useByteAsInt(boolean enable) {
    builder.useByteAsInt(enable);
    return this;
  }

  public JsonEncoderBuilder failOnMissingProperty(boolean enable) {
    builder.failOnMissingProperty(enable);
    return this;
  }

  public JsonEncoderBuilder useClassMetadataWithStaticType(boolean enable) {
    builder.useClassMetadataWithStaticType(enable);
    return this;
  }

  public JsonEncoderBuilder acceptSingleValueAsList(boolean enable) {
    builder.acceptSingleValueAsList(enable);
    return this;
  }

  public JsonEncoderBuilder useDefaultValue(Object value, Class<?> targetType) {
    builder.useDefaultValue(value, targetType);
    return this;
  }

  public JsonEncoderBuilder wrapRootValues(String inputKey, String outputKey) {
    builder.wrapRootValues(inputKey, outputKey);
    return this;
  }

  public JsonEncoderBuilder failOnNullPrimitive(boolean enabled) {
    builder.failOnNullPrimitive(enabled);
    return this;
  }

  public JsonEncoderBuilder useRuntimePropertyFilter(RuntimePropertyFilter filter) {
    builder.useRuntimePropertyFilter(filter);
    return this;
  }

  public JsonEncoder create() {
    return new JsonEncoder(builder.create());
  }

  public List<Factory<?>> getFactories() {
    return builder.getFactories();
  }

  public boolean isDateAsTimestamp() {
    return builder.isDateAsTimestamp();
  }
}
