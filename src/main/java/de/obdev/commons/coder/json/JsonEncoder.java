package de.obdev.commons.coder.json;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import de.obdev.commons.convert.Convertable;
import de.obdev.commons.model.ModelElement;
import de.obdev.commons.util.TypeDef;

public class JsonEncoder {

  Genson genson;

  public JsonEncoder(Genson genson) {
    this.genson = genson;
  }

  public Convertable encode(Object o) {
    if (o instanceof ModelElement) o = ((ModelElement) o).asRaw(); //IMPORTANT
    return Convertable.of(genson.serialize(o));
  }

  public <T> Convertable encode(Object o, TypeDef<T> type) {
    GenericType<T> genericType = new GenericType<T>() {};
    if (o instanceof ModelElement) o = ((ModelElement) o).asRaw(); //IMPORTANT
    return Convertable.of(genson.serialize(o, genericType));
  }
}
