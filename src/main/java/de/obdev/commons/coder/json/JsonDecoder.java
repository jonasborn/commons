package de.obdev.commons.coder.json;

import com.owlike.genson.GenericType;
import com.owlike.genson.Genson;
import de.obdev.commons.convert.Convertable;
import de.obdev.commons.util.TypeDef;

public class JsonDecoder {

  Genson genson;

  public JsonDecoder(Genson genson) {
    this.genson = genson;
  }

  public <T> T decode(String json, TypeDef<T> type) {
    GenericType<T> genericType = new GenericType<T>() {};
    return genson.deserialize(json, genericType);
  }

  public <T> T decode(String json) {
    GenericType<T> genericType = new GenericType<T>() {};
    return genson.deserialize(json, genericType);
  }

  public <T> T decode(Convertable resource, TypeDef<T> type) {
    GenericType<T> genericType = new GenericType<T>() {};
    return genson.deserialize(resource.asString(), genericType);
  }

  public <T> T decode(Convertable resource) {
    GenericType<T> genericType = new GenericType<T>() {};
    return genson.deserialize(resource.asString(), genericType);
  }
}
