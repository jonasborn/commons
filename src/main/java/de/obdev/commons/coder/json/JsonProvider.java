package de.obdev.commons.coder.json;

import com.owlike.genson.GensonBuilder;

public class JsonProvider {

  static GensonBuilder DEFAULT_PROVIDER;

  static {
    DEFAULT_PROVIDER = new GensonBuilder();
  }

  public static GensonBuilder getDefaultProvider() {
    return DEFAULT_PROVIDER;
  }

  public static void setDefaultProvider(GensonBuilder defaultProvider) {
    JsonProvider.DEFAULT_PROVIDER = defaultProvider;
  }
}
