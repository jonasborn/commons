package de.obdev.commons.coder.yaml;

import de.obdev.commons.io.ReadableResource;
import de.obdev.commons.util.TypeDef;
import org.yaml.snakeyaml.Yaml;

public class YamlDecoder {

  static Yaml yaml = new Yaml();

  public <T> T decode(ReadableResource resource, TypeDef<T> typeDef) {
    return yaml.loadAs(resource.stream(), typeDef.getRawClass());
  }

  public <T> T decode(ReadableResource resource) {
    TypeDef<T> typeDef = new TypeDef<T>() {};
    return decode(resource, typeDef);
  }

  public <T> T decode(String s, TypeDef<T> typeDef) {
    return yaml.loadAs(s, typeDef.getRawClass());
  }

  public <T> T decode(String s) {
    TypeDef<T> typeDef = new TypeDef<T>() {};
    return decode(s, typeDef);
  }
}
