package de.obdev.commons.coder.yaml;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.coder.json.JsonEncoder;
import de.obdev.commons.convert.Convertable;
import de.obdev.commons.util.TypeDef;
import org.yaml.snakeyaml.Yaml;

public class YamlEncoder {

  Yaml yaml = new Yaml();
  JsonEncoder encoder = Encoders.json().create();

  public <T> Convertable encode(Object o, TypeDef<T> type) {
    return Convertable.of(yaml.dump(o));
  }

  public Convertable encode(Object o) {
    return encode(o, null);
  }
}
