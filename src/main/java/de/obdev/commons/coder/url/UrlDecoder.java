package de.obdev.commons.coder.url;

import de.obdev.commons.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.Charset;

public class UrlDecoder {

  static Logger logger = LogManager.getLogger();

  public String decode(String s, Charset charset) throws UnsupportedEncodingException {
    return URLDecoder.decode(s, charset.toString());
  }

  public String decode(String s) {
    try {
      return decode(s, Charsets.DEFAULT);
    } catch (UnsupportedEncodingException e) {
      logger.warn("Unable to decode using default charset");
      return "";
    }
  }
}
