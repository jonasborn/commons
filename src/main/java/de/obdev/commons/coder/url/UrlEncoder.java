package de.obdev.commons.coder.url;

import de.obdev.commons.Charsets;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.Charset;

public class UrlEncoder {

  static Logger logger = LogManager.getLogger();

  public String encode(String source, Charset charset) throws UnsupportedEncodingException {
    return URLEncoder.encode(source, charset.toString());
  }

  public String encode(String source) {
    try {
      return encode(source, Charsets.DEFAULT);
    } catch (Exception e) {
      logger.warn("Unable to encode using default charset");
      return "";
    }
  }
}
