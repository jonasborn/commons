package de.obdev.commons.coder.base;

import com.google.common.io.BaseEncoding;
import de.obdev.commons.convert.Convertable;

public class Base64Encoder {

  public String asString(byte[] input) {
    return BaseEncoding.base64().encode(input);
  }

  public String asString(Convertable convertable) {
    return BaseEncoding.base64().encode(convertable.getBytes());
  }
}
