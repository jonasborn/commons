package de.obdev.commons.coder.base;

import com.google.common.io.BaseEncoding;

public class Base64Decoder {

  public byte[] decode(CharSequence base) {
    return BaseEncoding.base64().decode(base);
  }
}
