package de.obdev.commons.coder;

import de.obdev.commons.coder.base.Base64Decoder;
import de.obdev.commons.coder.json.JsonDecoderBuilder;
import de.obdev.commons.coder.model.ModelDecoder;
import de.obdev.commons.coder.yaml.YamlDecoder;

public class Decoders {

  public static JsonDecoderBuilder json() {
    return new JsonDecoderBuilder();
  }

  public static Base64Decoder base64() {
    return new Base64Decoder();
  }

  public static YamlDecoder yaml() {
    return new YamlDecoder();
  }

  public static ModelDecoder model() {
    return new ModelDecoder();
  }
}
