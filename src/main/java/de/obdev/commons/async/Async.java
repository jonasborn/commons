package de.obdev.commons.async;

import de.obdev.commons.util.Nullable;
import de.obdev.commons.util.UnsafeFunction;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.*;

public class Async implements Runnable {

  static Logger logger = LogManager.getLogger();

  static ExecutorService service = Executors.newCachedThreadPool();

  static ConcurrentHashMap<Future, AsyncResult> storage = new ConcurrentHashMap<>();

  /**
   * Runs some code async in background
   *
   * @param function A function with direct result (produced if finished) and a AsyncResult as
   *     parameter
   * @param <R> Something to return
   * @return
   */
  @Nullable
  public static <R> AsyncResult<R> run(UnsafeFunction<AsyncResult<R>, R> function) {
    AsyncResult<R> result = new AsyncResult<>();
    Future future = service.submit(() -> function.handle(result));
    storage.put(future, result);
    return result;
  }

  public static AsyncResult run(Runnable runnable, AsyncResult result) {
    Future future = service.submit(runnable);
    storage.put(future, result);
    return result;
  }

  static {
    new Thread(new Async()).start();
  }

  @SuppressWarnings("unchecked")
  @Override
  public void run() {
    logger.info("Started loop for {}", Async.class);
    while (true) {
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      Map<Future, AsyncResult> storage = new HashMap<>(Async.storage);
      List<Map.Entry<Future, AsyncResult>> trash = new ArrayList<>();
      for (Map.Entry<Future, AsyncResult> entry : storage.entrySet()) {
        if (entry.getKey().isDone()) {
          try {
            entry.getValue().succeed(entry.getKey().get());
          } catch (InterruptedException | ExecutionException e) {
            entry.getValue().fail(e);
          }
          trash.add(entry);
        }
        if (entry.getKey().isCancelled()) {
          entry.getValue().fail(new Exception("Callable was canceled"));
          trash.add(entry);
        }
      }
      Async.storage.entrySet().removeIf(trash::contains);
      trash.clear();
    }
  }
}
