package de.obdev.commons.async;

import de.obdev.commons.util.Nullable;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class AsyncResult<R> {

  CountDownLatch latch = new CountDownLatch(1);
  R result;
  Exception e;

  public AsyncResult() {}

  public void fail(Exception e) {
    this.e = e;
  }

  public void succeed() {
    latch.countDown();
  }

  public void succeed(R result) {
    this.result = result;
    succeed();
  }

  @Nullable
  public R get() {
    return result;
  }

  public boolean isPresent() {
    return result != null;
  }

  public void rewind() {
    latch = new CountDownLatch(1);
  }

  public boolean finished() {
    return latch.getCount() < 1;
  }

  public AsyncResult<R> await() throws Exception {
    try {
      latch.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    if (e != null) throw e;
    return this;
  }

  public AsyncResult<R> await(long timout, TimeUnit unit) throws Exception {
    try {
      latch.await(timout, unit);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    if (e != null) throw e;
    return this;
  }
}
