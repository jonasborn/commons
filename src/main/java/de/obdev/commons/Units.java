package de.obdev.commons;

import java.util.concurrent.TimeUnit;

public class Units {

  public static TimeUnit MICROSECONDS = TimeUnit.MICROSECONDS;
  public static TimeUnit MILLISECONDS = TimeUnit.MILLISECONDS;
  public static TimeUnit SECONDS = TimeUnit.SECONDS;
  public static TimeUnit MINUTES = TimeUnit.MINUTES;
  public static TimeUnit HOURS = TimeUnit.HOURS;
  public static TimeUnit DAYS = TimeUnit.DAYS;
}
