package de.obdev.commons.template;

import org.jtwig.JtwigTemplate;
import org.jtwig.environment.DefaultEnvironmentConfiguration;
import org.jtwig.environment.EnvironmentConfiguration;

import java.io.File;

public class Templates {

  static EnvironmentConfiguration configuration = new DefaultEnvironmentConfiguration();

  public static Template format(File file) {
    JtwigTemplate template = JtwigTemplate.fileTemplate(file, configuration);
    return new Template(template);
  }

  public static Template format(String s) {
    JtwigTemplate template = JtwigTemplate.inlineTemplate(s, configuration);
    return new Template(template);
  }
}
