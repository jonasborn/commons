package de.obdev.commons.template;

import de.obdev.commons.model.Model;
import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import java.io.OutputStream;

public class Template {

  JtwigTemplate base;

  public Template(JtwigTemplate base) {
    this.base = base;
  }

  public void render(Model model, OutputStream out) {
    JtwigModel jtwigModel = JtwigModel.newModel(model);
    base.render(jtwigModel, out);
  }

  public String render(Model model) {
    JtwigModel jtwigModel = JtwigModel.newModel(model);
    return base.render(jtwigModel);
  }
}
