package de.obdev.commons.config.handler;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import de.obdev.commons.Charsets;
import de.obdev.commons.config.PhysicalConfigCoder;
import org.yaml.snakeyaml.Yaml;

import java.util.Map;

public class YamlConfigCoder implements PhysicalConfigCoder {

  static Yaml yaml = new Yaml();
  static Gson gson = new Gson();

  @Override
  public JsonObject decode(byte[] array) throws Exception {
    String data = new String(array, Charsets.DEFAULT);
    Map<String, Object> map = yaml.load(data);
    System.out.println(gson.toJson(map));
    return gson.fromJson(gson.toJson(map), JsonObject.class);
  }

  @Override
  public byte[] encode(JsonObject object) throws Exception {
    Map map = gson.fromJson(object, Map.class);
    return yaml.dump(map).getBytes(Charsets.DEFAULT);
  }
}
