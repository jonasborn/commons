package de.obdev.commons.config.handler;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import de.obdev.commons.Charsets;
import de.obdev.commons.config.PhysicalConfigCoder;

public class JsonConfigCoder implements PhysicalConfigCoder {

  static Gson gson = new GsonBuilder().setPrettyPrinting().create();

  @Override
  public JsonObject decode(byte[] array) throws Exception {
    return gson.fromJson(new String(array, Charsets.DEFAULT), JsonObject.class);
  }

  @Override
  public byte[] encode(JsonObject object) throws Exception {
    return object.toString().getBytes(Charsets.DEFAULT);
  }
}
