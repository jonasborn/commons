package de.obdev.commons.config.mongodb;

import com.mongodb.MongoCredential;
import de.obdev.commons.database.MongoConfigElement;

public class MongoConfigCredential implements MongoConfigElement {

  String username;
  String database;
  String password;

  public MongoConfigCredential(String username, String database, String password) {
    this.username = username;
    this.database = database;
    this.password = password;
  }

  public String getUsername() {
    return username;
  }

  public String getDatabase() {
    return database;
  }

  public String getPassword() {
    return password;
  }

  public MongoCredential asMongoCredential() {
    return MongoCredential.createCredential(username, database, password.toCharArray());
  }
}
