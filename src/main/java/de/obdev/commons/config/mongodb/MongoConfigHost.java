package de.obdev.commons.config.mongodb;

import com.mongodb.ServerAddress;
import de.obdev.commons.database.MongoConfigElement;

public class MongoConfigHost implements MongoConfigElement {

  String host;
  Integer port;

  public MongoConfigHost(String host, Integer port) {
    this.host = host;
    this.port = port;
  }

  public String getHost() {
    return host;
  }

  public Integer getPort() {
    return port;
  }

  public ServerAddress asServerAdress() {
    return new ServerAddress(host, port);
  }
}
