package de.obdev.commons.config;

import de.obdev.commons.coder.Encoders;
import de.obdev.commons.convert.Convertable;
import de.obdev.commons.io.Streams;
import de.obdev.commons.util.TypeDef;

import java.io.File;
import java.io.FileNotFoundException;

public class Configs {

  static Config config;

  static TypeDef<? extends Config> typeDef = null;

  static File file = new File("config.model");

  public static <T extends Config> T getConfig() {
    typeDef = new TypeDef<T>() {};
    return (T) config;
  }

  public static void store() throws FileNotFoundException {
    Convertable convertable = Encoders.json().create().encode(config, typeDef);
    Streams.to(file).write(convertable);
  }

  public static void read() throws Exception {
    //config = Decoders.json().create().decode(Streams.from(file), typeDef);
    //TODO FIX!!
  }
}
