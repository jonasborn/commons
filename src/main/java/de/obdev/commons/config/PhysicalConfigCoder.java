package de.obdev.commons.config;

import com.google.gson.JsonObject;

public interface PhysicalConfigCoder {

  public JsonObject decode(byte[] array) throws Exception;

  public byte[] encode(JsonObject object) throws Exception;
}
