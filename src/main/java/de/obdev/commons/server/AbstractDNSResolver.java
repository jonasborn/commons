package de.obdev.commons.server;

import java.net.InetAddress;

@FunctionalInterface
public interface AbstractDNSResolver {

    public InetAddress resolve(String domain);

}
