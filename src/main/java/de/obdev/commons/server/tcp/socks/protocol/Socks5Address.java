package de.obdev.commons.server.tcp.socks.protocol;

import de.obdev.commons.server.AbstractDNSResolver;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class Socks5Address {

    AbstractDNSResolver resolver;
    Socks5ATYP atyp;
    byte[] raw;
    InetAddress address;

    public Socks5Address(AbstractDNSResolver resolver, Socks5ATYP atyp, byte[] raw) {
        this.resolver = resolver;
        this.atyp = atyp;
        this.raw = raw;

        switch (atyp) {
            case IPV4:
                try {
                    address = InetAddress.getByAddress(raw);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                break;
            case IPV6:
                try {
                    address = InetAddress.getByAddress(raw);
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                break;
            case DOMAIN:
                address = resolver.resolve(new String(raw));
                break;
        }
    }

    public Socks5ATYP getAtyp() {
        return atyp;
    }

    public byte[] getRaw() {
        return raw;
    }

    public InetAddress getInetAdress() {
        return address;
    }
}
