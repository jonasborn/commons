package de.obdev.commons.server.tcp.socks.protocol;

import java.io.ByteArrayOutputStream;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;

public class Socks5AuthRequest {

    byte version = 0x05;
    int nMethod;
    List<Socks5Method> methods = new ArrayList<>();

    public Socks5AuthRequest(byte nMethod, List<Socks5Method> methods) {
        this.version = version;
        this.nMethod = nMethod;
        this.methods = methods;
    }

    public Socks5AuthRequest(byte[] bytes) {
        ByteBuffer buffer = ByteBuffer.wrap(bytes);
        version = buffer.get();
        nMethod = (int) buffer.get();
        for (int i = 0; i < nMethod; i++) {
            try {
                methods.add(Socks5Method.valueOf(buffer.get()));
            } catch (Exception ignored) {}
        }
    }

    public int getnMethod() {
        return nMethod;
    }

    public List<Socks5Method> getMethods() {
        return methods;
    }

    public Socks5AuthRequest addMethod(Socks5Method m) {
        methods.add(m);
        nMethod = methods.size();
        return this;
    }

    public byte[] toByteArray() {
        ByteArrayOutputStream bout = new ByteArrayOutputStream(nMethod + 5);
        bout.write(version);
        bout.write((byte) nMethod);
        for (Socks5Method method:methods) {
            bout.write(method.getByte());
        }
        return bout.toByteArray();
    }
}
