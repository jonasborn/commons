package de.obdev.commons.server.tcp;

import de.obdev.commons.util.Nullable;

import java.util.HashMap;
import java.util.Map;

public class SocketSession {

    Map<String, Object> storage = new HashMap<>();

    public <T> T get(String key) {
        try {
            T result =  (T) storage.get(key);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    @Nullable
    public <T> T get(String key, Class<T> tClass) {
        try {
            T result =  (T) storage.get(key);
            return result;
        } catch (Exception e) {
            return null;
        }
    }

    public SocketSession set(String key, Object o) {
        this.storage.put(key, o);
        return this;
    }

    public <T> T orDefault(String key, T def) {
        T result;
        try {
            result = get(key);
        } catch (Exception e) {
            result = null;
        }
        if (result == null) return def;
        return result;
    }

    public boolean present(String... keys) {
        boolean status = true;
        for (int i = 0; i < keys.length; i++) {
            if (!storage.containsKey(keys[i])) status = false;
        }
        return status;
    }

    public boolean is(String key, Class<?> cls) {
        Object res = get(key);
        if (res == null) return false;
        if (!cls.isAssignableFrom(res.getClass())) return false;
        return true;
    }

    public SocketSession remove(String... keys) {
        for (int i = 0; i < keys.length; i++) {
            storage.remove(keys[i]);
        }
        return this;
    }

}
