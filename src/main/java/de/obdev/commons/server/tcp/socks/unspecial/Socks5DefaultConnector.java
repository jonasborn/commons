package de.obdev.commons.server.tcp.socks.unspecial;


import de.obdev.commons.server.tcp.SocketHandler;
import de.obdev.commons.server.tcp.SocketManager;
import de.obdev.commons.server.tcp.socks.protocol.Socks5CMD;
import de.obdev.commons.server.tcp.socks.protocol.Socks5Request;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.Socket;

public class Socks5DefaultConnector implements SocketHandler {

    static Logger logger = LogManager.getLogger();

    @Override
    public byte[] handle(SocketManager parent, byte[] input) {
        Socks5Request request = new Socks5Request(parent.getDnsResolver(), input);
        parent.getSession().set("target.address", request.getAddress());
        parent.getSession().set("target.port", request.getPort());
        try {
            Socket socket = new Socket(request.getAddress().getInetAdress(), request.getPort());
            parent.getSession().set("target.socket", socket);
            byte[] result =  request.setCmd(Socks5CMD.SUCCEEDED).toByteArray();
            logger.debug("Successfully connected to {}", request.getAddress().getInetAdress());
            return result;
        } catch (IOException e) {
            logger.info("Unable to connect to {}: {}", request.getAddress().getInetAdress(), e);
            request.setCmd(Socks5CMD.REFUSED);
            try {
                return request.toByteArray();
            } catch (IOException e1) {
                parent.stop();
            }
        }
        return new byte[0];
    }

}
