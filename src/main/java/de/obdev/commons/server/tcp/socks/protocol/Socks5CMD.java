package de.obdev.commons.server.tcp.socks.protocol;

import java.util.HashMap;
import java.util.Map;

public enum Socks5CMD {
    BIND((byte) 0x01), CONNECT((byte) 0x02), UDP((byte) 0x03),

    SUCCEEDED((byte) 0x00), SERVER_ERROR((byte) 0x01), UNALLOWED((byte) 0x02),
    UNREACHABLE_NETWORK((byte) 0x03), UNREACHABLE_HOST((byte) 0x04), REFUSED((byte) 0x05),
    TTL_EXPIRED((byte) 0x06), NOT_SUPPORTED((byte) 0x07), ATYPE_NOT_SUPPORTED((byte) 0x08);


    static Map<Byte, Socks5CMD> storage = new HashMap<>();

    static {
        for (Socks5CMD c: Socks5CMD.values()) {
            storage.put(c.getByte(), c);
        }
    }

    byte aByte;

    Socks5CMD(byte aByte) {

    }

    public byte getByte() {
        return aByte;
    }

    public static Socks5CMD valueOf(byte b) {
        return storage.get(b);
    }
}

