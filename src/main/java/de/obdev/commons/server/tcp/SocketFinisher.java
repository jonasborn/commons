package de.obdev.commons.server.tcp;

import java.io.InputStream;
import java.io.OutputStream;

@FunctionalInterface
public interface SocketFinisher {

    public void handle(SocketManager manager, InputStream is, OutputStream os);

}
