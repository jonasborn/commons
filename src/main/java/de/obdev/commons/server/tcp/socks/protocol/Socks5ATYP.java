package de.obdev.commons.server.tcp.socks.protocol;

import java.util.HashMap;
import java.util.Map;

public enum Socks5ATYP {
    IPV4((byte) 0x01), DOMAIN((byte) 0x03), IPV6((byte) 0x04);

    static Map<Byte, Socks5ATYP> storage = new HashMap<>();

    static {
        for (Socks5ATYP c: Socks5ATYP.values()) {
            System.out.println(c.getByte());
            storage.put(c.getByte(), c);
        }
    }

    byte aByte;

    Socks5ATYP(byte aByte) {
        this.aByte = aByte;
    }

    public byte getByte() {
        return aByte;
    }

    public static Socks5ATYP valueOf(byte b) {
        return storage.get(b);
    }
}

