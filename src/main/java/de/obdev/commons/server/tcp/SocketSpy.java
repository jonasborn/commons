package de.obdev.commons.server.tcp;

@FunctionalInterface
public interface SocketSpy {

    public void handle(SocketManager manager, byte[] read);

}
