package de.obdev.commons.server.tcp;

@FunctionalInterface
public interface SocketHandler {

    public byte[] handle(SocketManager parent, byte[] input);


}
