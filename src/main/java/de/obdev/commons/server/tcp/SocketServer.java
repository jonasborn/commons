package de.obdev.commons.server.tcp;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketServer implements Runnable {

    static Logger logger = LogManager.getLogger();

    int port;

    static ExecutorService service = Executors.newCachedThreadPool();


    ServerSocket server;

    //Map<Integer, SocketHandler> handlers = new HashMap<>();

    SocketHandler last;

    Class<? extends SocketManager> manager;

    public SocketServer(int port) {
        this.port = port;
    }

    public SocketServer start(Class<? extends SocketManager> manager) throws IOException {
        this.manager = manager;
        this.server = new ServerSocket(port);
        service.submit(this);
        System.out.println("STARTED");
        return  this;

    }



    @Override
    public void run() {

        logger.info("Server started on port {}", port);

        try {
            Socket accepted = server.accept();

            SocketManager man = manager.getConstructor().newInstance();
            man.init(accepted);
            service.submit(man);
        } catch (IOException e) {
            logger.info("Unable to accept request", e );
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
            logger.info("Unable to create new manager instance:", e);
        }

    }

    public SocketServer stop() {
        try {
            server.close();
        } catch (IOException ignored) {

        }
        service.shutdown();
        return this;
    }
}
