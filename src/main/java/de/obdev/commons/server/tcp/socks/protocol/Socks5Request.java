package de.obdev.commons.server.tcp.socks.protocol;

import de.obdev.commons.server.AbstractDNSResolver;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

public class Socks5Request {

    protected transient AbstractDNSResolver resolver;

    byte version = 0x05;
    Socks5CMD cmd;
    byte res = 0x00;
    Socks5ATYP atyp;
    Socks5Address address;
    int port;

    public Socks5Request(byte version, Socks5CMD cmd, byte res, Socks5ATYP atyp, Socks5Address address, int port) {
        this.version = version;
        this.cmd = cmd;
        this.res = res;
        this.atyp = atyp;
        this.address = address;
        this.port = port;
    }

    public Socks5Request(Socks5Request source) {
        this.version = source.version;
        this.cmd = source.cmd;
        this.res = source.res;
        this.atyp = source.atyp;
        this.address = source.address;
        this.port = source.port;
    }

    public Socks5Request(AbstractDNSResolver resolver, byte[] bytes) {
        this.resolver = resolver;
        ByteBuffer buffer = ByteBuffer.wrap(bytes);

        version = buffer.get();
        cmd = Socks5CMD.valueOf(buffer.get());
        res = buffer.get();
        atyp = Socks5ATYP.valueOf(buffer.get());



        switch (atyp) {
            case IPV4:
                byte[] ipv4 = new byte[4];
                buffer.get(ipv4);
                address = new Socks5Address(resolver, atyp, ipv4);
                break;
            case IPV6:
                byte[] ipv6 = new byte[16];
                buffer.get(ipv6);
                address = new Socks5Address(resolver, atyp, ipv6);
                break;
            case DOMAIN:
                int length = (int) buffer.get();
                byte[] domain = new byte[length];
                buffer.get(domain);
                address = new Socks5Address(resolver, atyp, domain);
                break;
        }

        port = ((buffer.get() & 0xff) << 8) | (buffer.get() & 0xff);
    }


    public byte[] toByteArray() throws IOException {
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        bout.write(version);
        bout.write(cmd.getByte());
        bout.write(res);
        bout.write(atyp.getByte());
        bout.write(address.getRaw());
        byte[] rawPort = new byte[2];
        rawPort[0] = (byte) (port & 0xFF);
        rawPort[1] = (byte) ((port >> 8) & 0xFF);
        bout.write(rawPort);
        return bout.toByteArray();
    }


    public byte getVersion() {
        return version;
    }

    public Socks5CMD getCmd() {
        return cmd;
    }

    public byte getRes() {
        return res;
    }

    public Socks5ATYP getAtyp() {
        return atyp;
    }

    public Socks5Address getAddress() {
        return address;
    }

    public int getPort() {
        return port;
    }

    public Socks5Request setVersion(byte version) {
        this.version = version;
        return this;
    }

    public Socks5Request setCmd(Socks5CMD cmd) {
        this.cmd = cmd;
        return this;
    }

    public Socks5Request setRes(byte res) {
        this.res = res;
        return this;
    }

    public Socks5Request setAtyp(Socks5ATYP atyp) {
        this.atyp = atyp;
        return this;
    }

    public Socks5Request setAddress(Socks5Address address) {
        this.address = address;
        return this;
    }

    public Socks5Request setPort(int port) {
        this.port = port;
        return this;
    }
}
