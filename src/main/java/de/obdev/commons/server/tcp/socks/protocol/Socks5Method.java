package de.obdev.commons.server.tcp.socks.protocol;

import java.util.HashMap;
import java.util.Map;

public enum Socks5Method {

    NO_AUTH_REQUIRED((byte) 0x00), GSSAPI((byte) 0x01), USERNAME_PASSWORD((byte) 0x02),
    NO_ACCEPTABLE_FOUND((byte) 0xFF);

    static Map<Byte, Socks5Method> storage = new HashMap<>();

    static {
        for (Socks5Method c: Socks5Method.values()) {
            storage.put(c.getByte(), c);
        }
    }

    byte aByte;

    Socks5Method(byte aByte) {
        this.aByte = aByte;
    }

    public byte getByte() {
        return aByte;
    }

    public static Socks5Method valueOf(byte b) {
        return storage.get(b);
    }

}
