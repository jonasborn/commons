package de.obdev.commons.server.tcp.socks.unspecial;


import de.obdev.commons.server.tcp.SocketHandler;
import de.obdev.commons.server.tcp.SocketManager;
import de.obdev.commons.server.tcp.socks.protocol.Socks5AuthRequest;
import de.obdev.commons.server.tcp.socks.protocol.Socks5AuthResponse;
import de.obdev.commons.server.tcp.socks.protocol.Socks5Method;

public class Socks5DefaultHandshaker implements SocketHandler {
    @Override
    public byte[] handle(SocketManager parent, byte[] input) {
        Socks5AuthRequest request = new Socks5AuthRequest(input);
        if (request.getMethods().contains(Socks5Method.NO_AUTH_REQUIRED)) {
            return new Socks5AuthResponse(Socks5Method.NO_AUTH_REQUIRED).toByteArray();
        } else {
            return new Socks5AuthResponse(Socks5Method.NO_ACCEPTABLE_FOUND).toByteArray();
        }
    }

}
