package de.obdev.commons.server.tcp.socks.unspecial;


import de.obdev.commons.server.tcp.SocketManager;

public class Socks5DefaultManager extends SocketManager {

    public Socks5DefaultManager() {
        this.add(0, new Socks5DefaultHandshaker());
        this.add(1, new Socks5DefaultConnector());
        this.add(new Socks5DefaultFinisher());
    }
}
