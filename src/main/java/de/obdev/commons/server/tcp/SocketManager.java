package de.obdev.commons.server.tcp;


import de.obdev.commons.primitives.Bytes;
import de.obdev.commons.server.AbstractDNSResolver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class SocketManager implements Runnable {

    static ExecutorService service = Executors.newCachedThreadPool();

    static Logger logger = LogManager.getLogger();

    static int DEFAULT_BUFFER_SIZE = 1024;

    public static int getDefaultBufferSize() {
        return DEFAULT_BUFFER_SIZE;
    }

    public static void setDefaultBufferSize(int defaultBufferSize) {
        DEFAULT_BUFFER_SIZE = defaultBufferSize;
    }

    Socket socket;

    InputStream is;
    OutputStream os;

    int max = Integer.MIN_VALUE;
    Map<Integer, SocketHandler> handlers = new HashMap<>();

    SocketSession session = new SocketSession();

    int position = 0;

    SocketFinisher last;

    int bufferSize = DEFAULT_BUFFER_SIZE;

    List<SocketSpy> spies = new ArrayList<>();

    AbstractDNSResolver dnsResolver = domain -> {
        try {
            return InetAddress.getByName(domain);
        } catch (UnknownHostException e) {
            return null;
        }
    };

    public SocketManager() {
    }

    public SocketManager setHandlers(Map<Integer, SocketHandler> handlers) {
        this.handlers = handlers;
        for (Integer i:handlers.keySet()) {
            if (i > max) max = i;
        }
        return this;
    }

    public SocketManager add(Integer position, SocketHandler handler) {
        this.handlers.put(position, handler);
        if (position > max) max = position;
        return this;
    }

    public SocketManager add(SocketFinisher last) {
        this.last = last;
        return this;
    }

    public SocketManager setLast(SocketFinisher last) {
        this.last = last;
        return this;
    }

    public SocketManager init(Socket socket) {
        this.socket = socket;
        return this;
    }

    public SocketManager setBufferSize(int bufferSize) {
        this.bufferSize = bufferSize;
        return this;
    }

    public int getBufferSize() {
        return bufferSize;
    }

    public SocketManager inform(byte[] input) {
        for (SocketSpy spy:spies) {
            service.submit(() -> spy.handle(this, input));
        }
        return this;
    }

    @Override
    public void run() {
        logger.debug("Accepted new socket");
        try {
            is = socket.getInputStream();
            os = socket.getOutputStream();
        } catch (IOException e) {
            Thread.currentThread().interrupt();
        }
        int read = 0;
        byte[] buffer = new byte[bufferSize];

        try {
            while (!Thread.currentThread().isInterrupted() && ((read = is.read(buffer)) != -1)) {
                byte[] current = Bytes.cut(buffer, read);
                inform(current);
                SocketHandler handler = handlers.get(position);

                if (handler != null) {
                    System.out.println(handler.getClass());
                    try {
                        byte[] result = handler.handle(this, current);
                        if (result != null) os.write(result);
                    } catch (Exception e) {
                        logger.warn("Unable to handle request with {}: {}", handler.getClass(), e);
                    }
                }
                position++;
                if (position>max) break;
            }
            if (Thread.interrupted()) stop();
            if (last != null) last.handle(this, is, os);
        } catch (Exception e) {
            //TODO LOGGER!
        }
    }

    public SocketManager stop() {
        try {
            this.socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Thread.currentThread().interrupt();
        return this;
    }


    public AbstractDNSResolver getDnsResolver() {
        return dnsResolver;
    }

    public boolean isAlive() {
        return !Thread.currentThread().isInterrupted();
    }

    public SocketSession getSession() {
        return session;
    }
}
