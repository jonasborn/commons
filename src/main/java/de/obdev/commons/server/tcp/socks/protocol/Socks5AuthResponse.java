package de.obdev.commons.server.tcp.socks.protocol;

import java.io.ByteArrayOutputStream;

public class Socks5AuthResponse {

    public static Socks5AuthResponse create(Socks5AuthRequest request) {
        if (request.getMethods().contains(Socks5Method.NO_AUTH_REQUIRED)) {
            return new Socks5AuthResponse(Socks5Method.NO_AUTH_REQUIRED);
        } else {
            return new Socks5AuthResponse(Socks5Method.NO_ACCEPTABLE_FOUND);
        }
    }

    byte version = 0x05;
    Socks5Method method;

    public Socks5AuthResponse(Socks5Method method) {
        this.method = method;
    }

    public byte[] toByteArray() {
        ByteArrayOutputStream bout  = new ByteArrayOutputStream(5);
        bout.write(version);
        bout.write(method.getByte());
        return bout.toByteArray();
    }
}
