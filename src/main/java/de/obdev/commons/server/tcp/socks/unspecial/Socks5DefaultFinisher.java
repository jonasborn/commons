package de.obdev.commons.server.tcp.socks.unspecial;


import de.obdev.commons.io.IOPipe;
import de.obdev.commons.server.tcp.SocketFinisher;
import de.obdev.commons.server.tcp.SocketManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Socks5DefaultFinisher implements SocketFinisher {

    @Override
    public void handle(SocketManager manager, InputStream is, OutputStream os) {
        Socket target = (Socket) manager.getSession().get("target.socket");
        try {
            new Thread(new IOPipe(is, target.getOutputStream())).start();
            new Thread(new IOPipe(target.getInputStream(), os)).start();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
