package de.obdev.commons.server;

import de.obdev.commons.server.tcp.SocketManager;
import de.obdev.commons.server.tcp.socks.unspecial.Socks5DefaultManager;

public class Handlers {

    public static Class<? extends SocketManager> defaultSocks5Manager() {
        return Socks5DefaultManager.class;
    }

}
