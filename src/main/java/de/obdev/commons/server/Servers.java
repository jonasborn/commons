package de.obdev.commons.server;

import de.obdev.commons.server.tcp.SocketServer;

public class Servers {

    public static SocketServer tcp(int port) {
        return new SocketServer(port);
    }

}
