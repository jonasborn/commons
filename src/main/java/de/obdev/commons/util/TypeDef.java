package de.obdev.commons.util;

import com.owlike.genson.reflect.TypeUtil;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;

public abstract class TypeDef<T> {

  private final Type type;
  private final Class<T> rawClass;

  protected TypeDef() {
    Type superType = this.getClass().getGenericSuperclass();
    if (superType instanceof Class) {
      throw new IllegalArgumentException("You must specify the parametrized type!");
    } else {
      this.type = ((ParameterizedType) superType).getActualTypeArguments()[0];
      this.rawClass = (Class<T>) TypeUtil.getRawClass(this.type);
    }
  }

  private TypeDef(Class<T> rawClass) {
    this.type = rawClass;
    this.rawClass = rawClass;
  }

  private TypeDef(Type type) {
    this.type = type;
    this.rawClass = (Class<T>) TypeUtil.getRawClass(type);
  }

  public static <T> TypeDef<T> of(Class<T> rawClass) {
    return new TypeDef<T>(rawClass) {};
  }

  public static TypeDef<Object> of(Type type) {
    return new TypeDef<Object>(type) {};
  }

  public Type getType() {
    return this.type;
  }

  public Class<T> getRawClass() {
    return this.rawClass;
  }
}
