package de.obdev.commons.util;

@FunctionalInterface
public interface UnsafeFunction<I, O> {
  public O handle(I i) throws Exception;
}
