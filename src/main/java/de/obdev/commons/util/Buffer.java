package de.obdev.commons.util;

import de.obdev.commons.convert.Convertable;

import java.nio.ByteBuffer;

public class Buffer {

  ByteBuffer buffer;

  public Buffer(byte[] bytes) {
    this.buffer = ByteBuffer.wrap(bytes);
  }

  public byte[] getBytes(int amount) {
    byte[] fin = new byte[amount];
    buffer.get(fin);
    return fin;
  }

  public Convertable get(int amount) {
    return Convertable.of(getBytes(amount));
  }
}
