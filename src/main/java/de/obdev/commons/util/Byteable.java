package de.obdev.commons.util;

import java.io.IOException;

public interface Byteable<T> {

  public byte[] asBytes() throws IOException;

  public T fromBytes(byte[] bytes);
}
