package de.obdev.commons.util;

@FunctionalInterface
public interface UnsafeConsumer<T> {
  public void handle(T t) throws Exception;
}
