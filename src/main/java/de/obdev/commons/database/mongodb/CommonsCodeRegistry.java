package de.obdev.commons.database.mongodb;

import de.obdev.commons.model.Model;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecRegistry;

import java.util.HashMap;
import java.util.Map;

public class CommonsCodeRegistry implements CodecRegistry {

  static Map<Class<?>, Codec<?>> codecs = new HashMap<>();

  static {
    codecs.put(Model.class, new ModelCodec());
  }

  @Override
  public <T> Codec<T> get(Class<T> aClass) {
    return (Codec<T>) codecs.get(aClass);
  }
}
