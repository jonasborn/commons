package de.obdev.commons.database.mongodb;

import com.mongodb.MongoNamespace;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.*;
import com.mongodb.client.model.*;
import com.mongodb.client.result.DeleteResult;
import de.obdev.commons.model.Model;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MCollection<T extends Bson> {

  MongoCollection<T> collection;

  public MCollection(MongoCollection<T> collection) {
    this.collection = collection;
  }

  public MongoNamespace namespace() {
    return collection.getNamespace();
  }

  /*public CodecRegistry getCodecRegistry() {
      return collection.getCodecRegistry();
  }

  public ReadPreference getReadPreference() {
      return collection.getReadPreference();
  }

  public WriteConcern getWriteConcern() {
      return collection.getWriteConcern();
  }

  public ReadConcern getReadConcern() {
      return collection.getReadConcern();
  }

  public <NewTMapObject> MongoCollection<NewTMapObject> withMapObjectClass(Class<NewTMapObject> aClass) {
      return collection.withMapObjectClass(aClass);
  }

  public MongoCollection<bson> withCodecRegistry(CodecRegistry codecRegistry) {
      return collection.withCodecRegistry(codecRegistry);
  }

  public MongoCollection<bson> withReadPreference(ReadPreference readPreference) {
      return collection.withReadPreference(readPreference);
  }

  public MongoCollection<bson> withWriteConcern(WriteConcern writeConcern) {
      return collection.withWriteConcern(writeConcern);
  }

  public MongoCollection<bson> withReadConcern(ReadConcern readConcern) {
      return collection.withReadConcern(readConcern);
  }*/

  public long count() {
    return collection.count();
  }

  public long count(Bson bson) {
    return collection.count(bson);
  }

  public long count(Bson bson, CountOptions countOptions) {
    return collection.count(bson, countOptions);
  }

  public <TResult> DistinctIterable<TResult> distinct(String s, Class<TResult> aClass) {
    return collection.distinct(s, aClass);
  }

  public <TResult> DistinctIterable<TResult> distinct(String s, Bson bson, Class<TResult> aClass) {
    return collection.distinct(s, bson, aClass);
  }

  public FindIterable<T> find() {
    return collection.find();
  }

  public <TResult> FindIterable<TResult> find(Class<TResult> aClass) {
    return collection.find(aClass);
  }

  public FindIterable<T> find(T bson) {
    return collection.find(bson);
  }

  public <TResult> FindIterable<TResult> find(Bson bson, Class<TResult> aClass) {
    return collection.find(bson, aClass);
  }

  public AggregateIterable<T> aggregate(List<? extends T> list) {
    return collection.aggregate(list);
  }

  public <TResult> AggregateIterable<TResult> aggregate(
      List<? extends Bson> list, Class<TResult> aClass) {
    return collection.aggregate(list, aClass);
  }

  public MapReduceIterable<T> mapReduce(String s, String s1) {
    return collection.mapReduce(s, s1);
  }

  public <TResult> MapReduceIterable<TResult> mapReduce(
      String s, String s1, Class<TResult> aClass) {
    return collection.mapReduce(s, s1, aClass);
  }

  public BulkWriteResult bulkWrite(List<? extends WriteModel<? extends T>> list) {
    return collection.bulkWrite(list);
  }

  public BulkWriteResult bulkWrite(
      List<? extends WriteModel<? extends T>> list, BulkWriteOptions bulkWriteOptions) {
    return collection.bulkWrite(list, bulkWriteOptions);
  }

  public MCollection insert(T... objects) {
    if (objects.length > 0) {
      if (objects.length == 1) {
        collection.insertOne(objects[0]);
      } else {
        collection.insertMany(Arrays.asList(objects));
      }
    }
    return this;
  }

  public static class DeleteBuilder<T extends Bson> {
    T bson;
    MongoCollection<T> collection;

    public DeleteBuilder(T bson, MongoCollection<T> collection) {
      this.bson = bson;
      this.collection = collection;
    }

    DeleteOptions options = new DeleteOptions();

    public DeleteBuilder collation(Collation collation) {
      options.collation(collation);
      return this;
    }

    public Collation collation() {
      return options.getCollation();
    }

    public DeleteResult one() {
      return collection.deleteOne(bson, options);
    }

    public DeleteResult many() {
      return collection.deleteMany(bson, options);
    }
  }

  public static class ReplaceBuilder<T extends Bson> {
    T bson;
    MongoCollection<T> collection;

    public ReplaceBuilder(T bson, MongoCollection<T> collection) {
      this.bson = bson;
      this.collection = collection;
    }

    UpdateOptions options = new UpdateOptions();

    public ReplaceBuilder collation(Collation collation) {
      options.collation(collation);
      return this;
    }

    public ReplaceBuilder bypassValidation(boolean status) {
      options.bypassDocumentValidation(status);
      return this;
    }

    public ReplaceBuilder upsert(boolean status) {
      options.upsert(status);
      return this;
    }

    public ReplaceBuilder with(T bson) {
      collection.replaceOne(this.bson, bson, options);
      return this;
    }
  }

  public DeleteBuilder delete(T bson) {
    return new DeleteBuilder(bson, collection);
  }

  public ReplaceBuilder replace(Bson bson) {
    return new ReplaceBuilder(bson, collection);
  }

  public static class UpdateBuilder<T extends Bson> {
    MongoCollection<T> collection;
    T bson;
    UpdateOptions options = new UpdateOptions();

    public UpdateBuilder(MongoCollection<T> collection, T bson) {
      this.collection = collection;
      this.bson = bson;
    }

    public UpdateBuilder upsert(boolean upsert) {
      options.upsert(upsert);
      return this;
    }

    public UpdateOptions bypassMapObjectValidation(Boolean bypassMapObjectValidation) {
      return options.bypassDocumentValidation(bypassMapObjectValidation);
    }

    public Collation getCollation() {
      return options.getCollation();
    }

    public UpdateBuilder collation(Collation collation) {
      options.collation(collation);
      return this;
    }

    public UpdateBuilder one(Bson update) {
      collection.updateOne(bson, update);
      return this;
    }

    public UpdateBuilder many(Bson update) {
      collection.updateOne(bson, update);
      return this;
    }
  }

  public UpdateBuilder update(T object) {
    return new UpdateBuilder(collection, object);
  }

  public static class FindOneBuilder<T extends Bson> {

    public FindOneBuilder(MongoCollection<T> collection, T query) {
      this.collection = collection;
      this.query = query;
    }

    MongoCollection<T> collection;
    T query;
    FindOneAndUpdateOptions updateOptions = new FindOneAndUpdateOptions();
    FindOneAndReplaceOptions replaceOptions = new FindOneAndReplaceOptions();
    FindOneAndDeleteOptions deleteOptions = new FindOneAndDeleteOptions();

    public FindOneBuilder sort(Model model) {
      Model res = model;
      updateOptions.sort(res);
      replaceOptions.sort(res);
      deleteOptions.sort(res);
      return this;
    }

    public FindOneBuilder collation(Collation collation) {
      updateOptions.collation(collation);
      replaceOptions.collation(collation);
      deleteOptions.collation(collation);
      return this;
    }

    public FindOneBuilder maxTime(long time, TimeUnit unit) {
      updateOptions.maxTime(time, unit);
      replaceOptions.maxTime(time, unit);
      deleteOptions.maxTime(time, unit);
      return this;
    }

    public FindOneBuilder projection(T object) {
      T res = object;
      updateOptions.projection(res);
      replaceOptions.projection(res);
      deleteOptions.projection(res);
      return this;
    }

    public FindOneBuilder bypassMapObjectValidation(boolean status) {
      updateOptions.bypassDocumentValidation(status);
      replaceOptions.bypassDocumentValidation(status);
      return this;
    }

    public FindOneBuilder upsert(boolean status) {
      updateOptions.upsert(status);
      replaceOptions.upsert(status);
      return this;
    }

    //TODO ADD REMAINING FUNCTIONS!

    public T delete() {
      return collection.findOneAndDelete(query, deleteOptions);
    }

    public T update(Bson update) {
      return collection.findOneAndUpdate(query, update, updateOptions);
    }

    public T replace(T replacement) {
      return collection.findOneAndReplace(query, replacement, replaceOptions);
    }
  }

  public FindOneBuilder findOne(T bson) {
    return new FindOneBuilder(collection, bson);
  }

  public void drop() {
    collection.drop();
  }

  public String createIndex(Bson bson) {
    return collection.createIndex(bson);
  }

  public String createIndex(Bson bson, IndexOptions indexOptions) {
    return collection.createIndex(bson, indexOptions);
  }

  public List<String> createIndexes(List<IndexModel> list) {
    return collection.createIndexes(list);
  }

  public ListIndexesIterable<Document> listIndexes() {
    return collection.listIndexes();
  }

  public <TResult> ListIndexesIterable<TResult> listIndexes(Class<TResult> aClass) {
    return collection.listIndexes(aClass);
  }

  public void dropIndex(String s) {
    collection.dropIndex(s);
  }

  public MCollection dropIndex(Model model) {
    collection.dropIndex(model);
    return this;
  }

  public MCollection dropIndexes() {
    collection.dropIndexes();
    return this;
  }

  public MCollection rename(MongoNamespace mongoNamespace) {
    collection.renameCollection(mongoNamespace);
    return this;
  }

  public MCollection rename(
      MongoNamespace mongoNamespace, RenameCollectionOptions renameCollectionOptions) {
    collection.renameCollection(mongoNamespace, renameCollectionOptions);
    return this;
  }
}
