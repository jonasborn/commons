package de.obdev.commons.database.mongodb;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.obdev.commons.config.mongodb.MongoConfigCredential;
import de.obdev.commons.config.mongodb.MongoConfigHost;
import de.obdev.commons.database.MongoConfigElement;
import de.obdev.commons.util.TypeDef;

import java.util.ArrayList;
import java.util.List;

public class MongoDB {

  List<ServerAddress> addresses = new ArrayList<>();
  List<MongoCredential> credentials = new ArrayList<>();

  MongoClient client;

  public MongoDB(MongoConfigElement... objects) {
    for (int i = 0; i < objects.length; i++) {
      MongoConfigElement element = objects[i];
      if (element instanceof MongoConfigHost) {
        addresses.add(((MongoConfigHost) element).asServerAdress());
      } else if (element instanceof MongoConfigCredential) {
        credentials.add(((MongoConfigCredential) element).asMongoCredential());
      }
    }

    client = new MongoClient(addresses, credentials);
  }

  public MongoDatabase get(String database) {
    return client.getDatabase(database).withCodecRegistry(new CommonsCodeRegistry());
  }

  public <T> MongoCollection<T> get(String database, String collection) {
    TypeDef<T> typeDef = new TypeDef<T>() {};
    return get(database).getCollection(collection, typeDef.getRawClass());
  }
}
