package de.obdev.commons.database.mongodb;

import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import de.obdev.commons.util.TypeDef;
import org.bson.conversions.Bson;

public class MDatabase {

  MongoDatabase database;

  public MDatabase(MongoDatabase database) {
    this.database = database;
  }

  public <T extends Bson> MCollection<T> get(String collection) {
    TypeDef<T> typeDef = new TypeDef<T>() {};
    MongoCollection<T> mongoCollection = database.getCollection(collection, typeDef.getRawClass());
    return new MCollection<T>(mongoCollection);
  }
}
