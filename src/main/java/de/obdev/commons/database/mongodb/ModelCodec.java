package de.obdev.commons.database.mongodb;

import de.obdev.commons.model.Model;
import org.bson.BsonReader;
import org.bson.BsonWriter;
import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.DecoderContext;
import org.bson.codecs.DocumentCodec;
import org.bson.codecs.EncoderContext;

public class ModelCodec implements Codec<Model> {

  DocumentCodec codec = new DocumentCodec();

  @Override
  public Model decode(BsonReader bsonReader, DecoderContext decoderContext) {
    return new Model(codec.decode(bsonReader, decoderContext));
  }

  @Override
  public void encode(BsonWriter bsonWriter, Model model, EncoderContext encoderContext) {
    Document document = new Document(model.asMap());
    codec.encode(bsonWriter, document, encoderContext);
  }

  @Override
  public Class<Model> getEncoderClass() {
    return Model.class;
  }
}
