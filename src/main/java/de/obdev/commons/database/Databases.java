package de.obdev.commons.database;

import de.obdev.commons.database.mongodb.MongoDB;

import java.util.HashMap;
import java.util.Map;

public class Databases {

  Map<String, MongoDB> mongoDatabases = new HashMap<>();

  public MongoDB mongo(String name) {
    return mongoDatabases.get(name);
  }
}
