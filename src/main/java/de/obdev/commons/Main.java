package de.obdev.commons;

import de.obdev.commons.crypto.Ciphers;
import de.obdev.commons.crypto.Keys;
import de.obdev.commons.crypto.asymmetric.AsymmetricKeyPair;
import de.obdev.commons.random.Randoms;

import javax.crypto.KeyGenerator;
import java.security.Key;
import java.security.KeyFactory;

public class Main {

    public static void main(String[] args) throws Exception {

        byte[] data = Randoms.unsecure().getBytes(16);

        AsymmetricKeyPair keyPair = Keys.asymmetricPairs().mcEliece().size(128).create();

        byte[] enc = Ciphers.asymmetric().mcEliece(keyPair.getShared()).encrypt(data);
        byte[] dec = Ciphers.asymmetric().mcEliece(keyPair.getSecret()).decrypt(enc);


    }
}
