package de.obdev.commons.convert;

import de.obdev.commons.Charsets;
import de.obdev.commons.coder.Encoders;
import de.obdev.commons.io.ReadableResource;
import de.obdev.commons.io.Streams;
import de.obdev.commons.util.Nullable;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.NumberFormat;
import java.text.ParseException;

public class Convertable {

  public static Convertable of(byte[] bytes) {
    return new Convertable(bytes);
  }

  public static Convertable of(short s) {
    ByteBuffer buffer = ByteBuffer.allocate(Short.BYTES);
    buffer.putShort(s);
    return new Convertable(buffer.array());
  }

  public static Convertable of(int i) {
    ByteBuffer buffer = ByteBuffer.allocate(Integer.BYTES);
    buffer.putLong(i);
    return new Convertable(buffer.array());
  }

  public static Convertable of(float f) {
    ByteBuffer buffer = ByteBuffer.allocate(Float.BYTES);
    buffer.putFloat(f);
    return new Convertable(buffer.array());
  }

  public static Convertable of(double d) {
    ByteBuffer buffer = ByteBuffer.allocate(Double.BYTES);
    buffer.putDouble(d);
    return new Convertable(buffer.array());
  }

  public static Convertable of(long l) {
    ByteBuffer buffer = ByteBuffer.allocate(Long.BYTES);
    buffer.putLong(l);
    return new Convertable(buffer.array());
  }

  public static Convertable of(char c) {
    ByteBuffer buffer = ByteBuffer.allocate(Character.BYTES);
    buffer.putChar(c);
    return new Convertable(buffer.array());
  }

  public static Convertable of(BigInteger integer) {
    return new Convertable(integer.toByteArray());
  }

  public static Convertable of(BigDecimal decimal) {
    return new Convertable(decimal.unscaledValue().toByteArray()); //TODO UNSAFE -> TEST
  }

  public static Convertable of(CharSequence sequence) {
    if (sequence instanceof String) {
      return of((String) sequence);
    } else {
      return of((String) String.valueOf(sequence));
    }
  }

  @Nullable
  public static Convertable of(Number number) {
    if (number instanceof Short) {
      return of((short) number);
    } else if (number instanceof Integer) {
      return of((int) number);
    } else if (number instanceof Float) {
      return of((float) number);
    } else if (number instanceof Double) {
      return of((double) number);
    } else if (number instanceof Long) {
      return of((long) number);
    } else if (number instanceof BigInteger) {
      return of((BigInteger) number);
    } else if (number instanceof BigDecimal) {
      return of((BigDecimal) number);
    }
    return null;
  }

  public static Convertable of(String s, Charset charset) {
    return new Convertable(s.getBytes(charset));
  }

  public static Convertable of(String s) {
    return new Convertable(s.getBytes(Charsets.DEFAULT));
  }

  public static ByteBuffer buffered(byte[] bytes) {
    return ByteBuffer.wrap(bytes);
  }

  byte[] bytes;
  ByteBuffer buffer;

  public Convertable(byte[] bytes) {
    this.bytes = bytes;
    buffer = buffered(bytes);
  }

  public ReadableResource asStream() {
    return Streams.from(bytes);
  }

  public ReadableResource asResource() {
    return asStream();
  }

  public short asShort() {
    return buffer.getShort();
  }

  public int asInt() {
    return buffer.getInt();
  }

  public float asFloat() {
    return buffer.getFloat();
  }

  public double asDouble() {
    return buffer.getDouble();
  }

  public long asLong() {
    return buffer.getLong();
  }

  public char asChar() {
    return buffer.getChar();
  }

  public byte[] getBytes() {
    return bytes;
  }

  public byte[] asBytes() {
    return bytes;
  }

  public byte asByte() {
    return bytes[0];
  }

  public String asString(Charset charset) {
    return new String(bytes, charset);
  }

  public String asString() {
    return asString(Charsets.DEFAULT);
  }

  public BigInteger asBigInteger() {
    return new BigInteger(bytes);
  }

  public BigDecimal asBigDecimal(Charset charset) {
    return new BigDecimal(asString(charset));
  }

  public BigDecimal asBigDecimal() {
    return asBigDecimal(Charsets.DEFAULT);
  }

  public Number asNumber(Charset charset) throws ParseException {
    return NumberFormat.getInstance().parse(asString(charset));
  }

  public Number asNumber() throws ParseException {
    return asNumber(Charsets.DEFAULT);
  }

  public String asBase64() {
    return Encoders.base64().asString(bytes);
  }
}
