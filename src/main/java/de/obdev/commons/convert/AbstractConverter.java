package de.obdev.commons.convert;

public abstract class AbstractConverter {

  public abstract Convertable handle(Convertable convertable);

  public Convertable getConvertable(Number number) {
    return handle(Convertable.of(number));
  }

  public Convertable getConvertable(byte[] bytes) {
    return handle(Convertable.of(bytes));
  }

  public Convertable getConvertable(CharSequence sequence) {
    return handle(Convertable.of(sequence));
  }

  public int getInt(Number source) {
    return getConvertable(source).asInt();
  }

  public int getInt(byte[] source) {
    return getConvertable(source).asInt();
  }

  public int getInt(CharSequence source) {
    return getConvertable(source).asInt();
  }

  // FLOAT

  public float getFloat(Number source) {
    return getConvertable(source).asFloat();
  }

  public float getFloat(byte[] source) {
    return getConvertable(source).asFloat();
  }

  public float getFloat(CharSequence source) {
    return getConvertable(source).asFloat();
  }

  //DOUBLE

  public double getDouble(Number source) {
    return getConvertable(source).asDouble();
  }

  public double getDouble(byte[] source) {
    return getConvertable(source).asDouble();
  }

  public double getDouble(CharSequence source) {
    return getConvertable(source).asDouble();
  }

  //LONG

  public long getLong(Number source) {
    return getConvertable(source).asLong();
  }

  public long getLong(byte[] source) {
    return getConvertable(source).asLong();
  }

  public long getLong(CharSequence source) {
    return getConvertable(source).asLong();
  }

  //CHAR

  public char getChar(Number source) {
    return getConvertable(source).asChar();
  }

  public char getChat(byte[] source) {
    return getConvertable(source).asChar();
  }

  public char getChat(CharSequence source) {
    return getConvertable(source).asChar();
  }

  //BYTE

  public byte[] getBytes(Number source) {
    return getConvertable(source).asBytes();
  }

  public byte[] getBytes(byte[] source) {
    return getConvertable(source).asBytes();
  }

  public byte[] getBytes(CharSequence source) {
    return getConvertable(source).asBytes();
  }
}
