package de.obdev.commons.crypto.asymmetric;

import de.obdev.commons.crypto.Ciphers;
import de.obdev.commons.crypto.Keys;
import de.obdev.commons.random.Randoms;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class McElieceTest {

    @Test
    public void testMcEliece128() throws Exception {

        byte[] data = Randoms.unsecure().getBytes(16);

        AsymmetricKeyPair keyPair = Keys.asymmetricPairs().mcEliece().size(128).create();

        byte[] enc = Ciphers.asymmetric().mcEliece(keyPair.getShared()).encrypt(data);
        byte[] dec = Ciphers.asymmetric().mcEliece(keyPair.getSecret()).decrypt(enc);

        assertEquals(Arrays.toString(data), Arrays.toString(dec));

    }

    @Test
    public void testMcEliece256() throws Exception {

        byte[] data = Randoms.unsecure().getBytes(16);

        AsymmetricKeyPair keyPair = Keys.asymmetricPairs().mcEliece().size(256).create();

        byte[] enc = Ciphers.asymmetric().mcEliece(keyPair.getShared()).encrypt(data);
        byte[] dec = Ciphers.asymmetric().mcEliece(keyPair.getSecret()).decrypt(enc);

        assertEquals(Arrays.toString(data), Arrays.toString(dec));
    }

    @Test
    public void testMcEliece512() throws Exception {

        byte[] data = Randoms.unsecure().getBytes(16);

        AsymmetricKeyPair keyPair = Keys.asymmetricPairs().mcEliece().size(512).create();

        byte[] enc = Ciphers.asymmetric().mcEliece(keyPair.getShared()).encrypt(data);
        byte[] dec = Ciphers.asymmetric().mcEliece(keyPair.getSecret()).decrypt(enc);

        assertEquals(Arrays.toString(data), Arrays.toString(dec));
    }


}
